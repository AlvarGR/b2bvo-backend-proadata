package grupoHasten.b2bvo.application.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import grupoHasten.b2bvo.application.dtos.ImporteDto;
import grupoHasten.b2bvo.application.service.impl.ImporteServiceImpl;

@SpringBootTest
public class ImporteServiceImplTest {
	
	@Autowired
	ImporteServiceImpl importeService;
	
	@Test
	public void calcularImporteCantidadTest() {
		
		ImporteDto importeTest = new ImporteDto();
		
		importeTest.setPrecioBase(10000.0);
		importeTest.setDescuentoCantidad(1000.0);
		
		ImporteDto importe = importeService.calcularImporte(importeTest);
		
		assertEquals(10.0, importe.getDescuentoPorcentaje());
		assertEquals(9000.0, importe.getPrecioFinal());
	}


	@Test
	public void calcularImporteDescuentofTest() {
		
		ImporteDto importeTest = new ImporteDto();

		importeTest.setPrecioBase(10000.0);
		importeTest.setDescuentoPorcentaje(10.0);

		ImporteDto importe = importeService.calcularImporte(importeTest);
		
		assertEquals(1000.0, importe.getDescuentoCantidad());
		assertEquals(9000.0, importe.getPrecioFinal());
	}
}
