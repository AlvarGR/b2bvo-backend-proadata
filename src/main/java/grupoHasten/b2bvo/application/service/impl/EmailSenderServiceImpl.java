package grupoHasten.b2bvo.application.service.impl;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.stereotype.Service;

import grupoHasten.b2bvo.application.service.EmailSenderService;

@Service("EmailSenderServiceImpl")
public class EmailSenderServiceImpl implements EmailSenderService {
    private final Properties properties = new Properties();

//    private String password;

    private Session session;

    private void init() {

	properties.put("mail.smtp.host", "smtp.ionos.es"); // mail.gmail.com
	properties.put("mail.smtp.starttls.enable", "true");
	properties.put("mail.smtp.port", 587);
	properties.put("mail.smtp.mail.sender", "b2bvo@grupohasten.es");
	properties.put("mail.smtp.user", "b2bvo@grupohasten.es");
	properties.put("mail.smtp.auth", "true");

	session = Session.getDefaultInstance(properties);
    }

    public void sendEmail(String email, String text) {
	init();
	try {
	    MimeMessage message = new MimeMessage(session);
	    // message.setFrom(new
	    // InternetAddress((String)properties.get("mail.smtp.mail.sender")));
	    message.setFrom(new InternetAddress((String) properties.get("mail.smtp.mail.sender")));
	    message.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
	    message.setSubject("B2BVO Restablecimiento de contraseña ");
	    message.setText(text);
	    Transport t = session.getTransport("smtp");
	    t.connect((String) properties.get("mail.smtp.user"), "P-CZ9!Cbq2S3W1+yka");
	    t.sendMessage(message, message.getAllRecipients());
	    t.close();
	} catch (MessagingException me) {
	    // Aqui se deberia o mostrar un mensaje de error o en lugar
	    // de no hacer nada con la excepcion, lanzarla para que el modulo
	    // superior la capture y avise al usuario con un popup, por ejemplo.
	    return;
	}
    }
}