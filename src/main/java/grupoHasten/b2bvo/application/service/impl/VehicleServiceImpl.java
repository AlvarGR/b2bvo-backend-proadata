package grupoHasten.b2bvo.application.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import grupoHasten.b2bvo.application.dtos.VehicleDto;
import grupoHasten.b2bvo.application.dtos.convert.VehicleConvert;
import grupoHasten.b2bvo.application.model.Attachment;
import grupoHasten.b2bvo.application.model.Engine;
import grupoHasten.b2bvo.application.model.Vehicle;
import grupoHasten.b2bvo.application.repository.AttachmentRepository;
import grupoHasten.b2bvo.application.repository.EngineRepository;
import grupoHasten.b2bvo.application.repository.VehicleRepository;
import grupoHasten.b2bvo.application.service.UserService;
import grupoHasten.b2bvo.application.service.VehicleService;
import grupoHasten.b2bvo.application.service.exception.VehicleNotFoundException;

@Service
public class VehicleServiceImpl implements VehicleService {

  @Value("${paginacion.numObjectForPage}")
  private int numObjectForPage;

  @Autowired
  VehicleRepository vehicleRepository;

  @Autowired
  EngineRepository engineRepository;

  @Autowired
  UserService userService;

  @Autowired
  AttachmentRepository attachmentRepository;

  @Autowired
  VehicleConvert vehicleConvert;

  @Override
  public VehicleDto saveVehicle(VehicleDto vehicleDto) {
    Vehicle vehicle = vehicleConvert.transformDtoToEntity(vehicleDto);

    Engine newEngine = vehicle.getEngine();
    Engine engine = engineRepository.save(newEngine);
    vehicle.setEngine(engine);
    if (vehicleDto.getUploader() != null) {
      vehicle.setUploader(userService.findUserById(vehicleDto.getUploader()).get());
    }
    for (Attachment item : vehicle.getAttachments()) {
      Attachment newAttachment = item;
      vehicle.deleteAttachment(item);
      Attachment attachment = attachmentRepository.save(newAttachment);
      vehicle.addAttachment(attachment);
    }

    Vehicle vehicleNew = vehicleRepository.save(vehicle);

    return vehicleConvert.transformEntityToDto(vehicleNew);
  }


  @Override
  public Page<VehicleDto> findVehicleByUserId(int pageIndex, Long userId) {
    Pageable pageable = PageRequest.of(pageIndex, numObjectForPage);
    Page<Vehicle> vehicles = this.vehicleRepository.findByUploader_Id(userId, pageable);
    return vehicleConvert.pageEntitiesToPageDto(vehicles);
  }

  public List<VehicleDto> getVehicles() {
    List<Vehicle> vehicles = vehicleRepository.findAll();
    List<VehicleDto> vehiclesDto = new ArrayList<>();
    for (Vehicle item : vehicles) {
      vehiclesDto.add(vehicleConvert.transformEntityToDto(item));
    }
    return vehiclesDto;
  }

  @Override
  public Vehicle getVehicleEntity(Long id) throws VehicleNotFoundException {
    Vehicle vehicle = vehicleRepository.findById(id).orElseThrow(
        () -> new VehicleNotFoundException(String.format("Vehicle with id = %d Not Found", id)));
    return vehicle;
  }


  @Override
  public VehicleDto getVehicle(Long id) throws VehicleNotFoundException {
    Vehicle vehicle = vehicleRepository.findById(id).orElseThrow(
        () -> new VehicleNotFoundException(String.format("Vehicle with id = %d Not Found", id)));
    return vehicleConvert.transformEntityToDto(vehicle);
  }


}
