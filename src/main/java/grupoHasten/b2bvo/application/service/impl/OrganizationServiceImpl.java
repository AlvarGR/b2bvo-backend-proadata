package grupoHasten.b2bvo.application.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import grupoHasten.b2bvo.application.dtos.convert.OrganizationConvert;
import grupoHasten.b2bvo.application.model.Organization;
import grupoHasten.b2bvo.application.repository.OrganizationRepository;
import grupoHasten.b2bvo.application.service.OrganizationService;

@Service
public class OrganizationServiceImpl implements OrganizationService{

    @Autowired
    OrganizationRepository organizationRepo;
    
    @Autowired
    OrganizationConvert organizationConvert;
    
    public Organization obtenerOrganizacion(Long id) {
	
	return organizationRepo.getOne(id);
    }
    
}
