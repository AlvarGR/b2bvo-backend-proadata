package grupoHasten.b2bvo.application.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import grupoHasten.b2bvo.application.dtos.MisBusquedasDto;
import grupoHasten.b2bvo.application.dtos.convert.MisBusquedasConvert;
import grupoHasten.b2bvo.application.model.MisBusquedas;
import grupoHasten.b2bvo.application.repository.MisBusquedasRepository;
import grupoHasten.b2bvo.application.service.MisBusquedasService;

@Service
public class MisBusquedasServiceImpl implements MisBusquedasService {
	
	@Autowired
	MisBusquedasRepository misBusquedasRepository;
	
	@Autowired
	MisBusquedasConvert misBusquedasConvert;

	@Override
	public List<MisBusquedasDto> getMisBusquedasAll(String user) {
		List<MisBusquedas> misBusquedas = misBusquedasRepository.findByUser(misBusquedasConvert.obtenerUser(user));
		
		return misBusquedasConvert.transformListEntitiesToListDto(misBusquedas);
	}

	@Override
	public MisBusquedasDto getMisBusquedas(Long id) {
		MisBusquedas misBusquedas = misBusquedasRepository.findById(id).get();
		
		return misBusquedasConvert.transformEntityToDto(misBusquedas);
	}

	@Override
	public MisBusquedasDto saveMisBusquedas(MisBusquedasDto misBusquedasDto) {
		MisBusquedas misBusquedas = misBusquedasConvert.transformDtoToEntity(misBusquedasDto);
		MisBusquedas misBusquedasNew = misBusquedasRepository.save(misBusquedas);
		
		return misBusquedasConvert.transformEntityToDto(misBusquedasNew);
	}

	@Override
	public void removeMisBusquedas(Long id) {
		misBusquedasRepository.deleteById(id);
	}


}
