package grupoHasten.b2bvo.application.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import grupoHasten.b2bvo.application.facade.impl.JatoFacadeImpl;
import grupoHasten.b2bvo.application.facade.model.JatoFilter;
import grupoHasten.b2bvo.application.facade.model.JatoMake;
import grupoHasten.b2bvo.application.facade.model.JatoMakeModel;
import grupoHasten.b2bvo.application.facade.model.JatoModel;
import grupoHasten.b2bvo.application.facade.model.JatoVersion;
import grupoHasten.b2bvo.application.service.JatoService;

@Service
public class JatoServiceImpl implements JatoService {

	@Autowired
	JatoFacadeImpl jatoFacade;
	
	@Override
	public List<JatoMake> obtenerMakes() {
		List<JatoMake> makes = jatoFacade.obtenerMakes();
		return makes;
	}

	@Override
	public List<JatoModel> obtenerModel(String make) {
		List<JatoModel> models = jatoFacade.obtenerModels(make);
		return models;
	}

	@Override
	public JatoFilter obtenerFilter(String make, String model) {
		return jatoFacade.obtenerFilter(make, model);
	}

	@Override
	public List<JatoVersion> obtenerVersion(String make, String model) {
		return jatoFacade.obtenerVersion(make, model);
	}

	@Override
	public List<JatoMakeModel> obtenerJatoMakeModel() {
		List<JatoMakeModel> makeModels = new ArrayList<>();
		
		List<JatoMake> makes = jatoFacade.obtenerMakes();
		for(JatoMake make: makes) {
			JatoMakeModel jatoMakeModel = new JatoMakeModel();
			jatoMakeModel.setMake(make.getMake());
			jatoMakeModel.setLocalMake(make.getLocalMake());
			List<JatoModel> models = jatoFacade.obtenerModels(make.getMake());
			jatoMakeModel.setModels(models);
			makeModels.add(jatoMakeModel);
		}
		
		return makeModels;
	}

}
