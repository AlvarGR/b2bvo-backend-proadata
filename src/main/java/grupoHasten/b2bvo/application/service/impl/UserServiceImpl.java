package grupoHasten.b2bvo.application.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import grupoHasten.b2bvo.application.model.ApplicationUser;
import grupoHasten.b2bvo.application.repository.IUserRepository;
import grupoHasten.b2bvo.application.service.UserService;
import grupoHasten.b2bvo.application.service.exception.ApplicationUserNotFoundException;
import grupoHasten.b2bvo.application.service.exception.VehicleNotFoundException;

@Service("userService")
//public class UserServiceImpl implements UserDetailsService{
public class UserServiceImpl implements UserService{

    @Autowired
    private IUserRepository userRepo;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserServiceImpl(IUserRepository userRepo, BCryptPasswordEncoder bCryptPasswordEncoder) {
	super();
	this.userRepo = userRepo;
	this.bCryptPasswordEncoder = bCryptPasswordEncoder; // , BCryptPasswordEncoder bCryptPasswordEncoder
    }

    public ApplicationUser existUser(String nameOrEmail, String password) {

	// Optional<ApplicationUser> optUsuario;
	ApplicationUser usuario = new ApplicationUser();

	// Obtener el usuario si existe
	try {
	    if (nameOrEmail.contains("@")) {
		usuario = userRepo.findByEmail(nameOrEmail);
	    } else {
		usuario = userRepo.findByName(nameOrEmail);
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
	// Comprobacion Password bCryptPasswordEncoder.encode(password)
	// passEncoder.encode(password)
	// //bCryptPasswordEncoder.encode(password).equals(usuario.getPassword())
	// usuario.getPassword() == bCryptPasswordEncoder.encode(password)
	// String PassAux = bCryptPasswordEncoder.encode(password);
	if (usuario != null && usuario.getPassword().equals(String.valueOf(password))) {
	    return usuario;
	} else {
	    return null;
	}
    }

    public List<ApplicationUser> listarUsuarios() {

	List<ApplicationUser> lstUsuarios = userRepo.findAll();

	return lstUsuarios;
    }

    public ApplicationUser saveUser(ApplicationUser usuario) {

	return userRepo.save(usuario);
    }

    public Optional<ApplicationUser> findUserById(Long id) {
	return userRepo.findById(id);
	
	//.orElseThrow(() -> new ApplicationUserNotFoundException(String.format("Vehicle with id = %d Not Found", id)));
    }
    
    public ApplicationUser findUserByResetToken(String resetToken) {
	return userRepo.findByResetToken(resetToken);
    }
    
    public ApplicationUser findUserByEmail(String userEmail) {
	// TODO Auto-generated method stub
	return userRepo.findByEmail(userEmail);
    }

    public void createPasswordResetTokenForUser(ApplicationUser user, String token) {
	user.setResetToken(token);
	try {
	    userRepo.save(user);
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }
    
    

}
