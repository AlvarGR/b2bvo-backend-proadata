package grupoHasten.b2bvo.application.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.s3.model.S3VersionSummary;
import com.amazonaws.services.s3.model.VersionListing;
import com.amazonaws.util.IOUtils;

import grupoHasten.b2bvo.application.dtos.ResponseBuckerDto;
import grupoHasten.b2bvo.application.service.AmazonClient;

@Service
public class AmazonClientImpl implements AmazonClient {

    @Autowired
    private AmazonS3 s3client;

    @Value("${amazonProperties.endpointUrl}")
    private String endpointUrl;
    
    @Value("${amazonProperties.bucketName}")
    private String bucketName;

	public ResponseBuckerDto uploadFile(MultipartFile multipartFile) throws IOException {
        String  status = null;
        
        File file = convertMultiPartToFile(multipartFile);
        
        String originalKey = multipartFile.getOriginalFilename();
        originalKey = originalKey.substring(0, originalKey.indexOf('.'));
        int index = 0;
        String key = String.format("%s.%03d", originalKey, index);
        while (existsFile(key)) {
        	key = String.format("%s.%03d", originalKey, ++index);
        }
        
        status = uploadFileTos3bucket(key, file);
        file.delete();
        
        return new ResponseBuckerDto(status,  key);
        
    }
	
	@Override
	public ResponseBuckerDto deleteFileFromS3Bucket(String key) {
		VersionListing versiones = s3client.listVersions(bucketName, key);
		
		Iterator<S3VersionSummary> versionIter = versiones.getVersionSummaries().iterator();
        while (versionIter.hasNext()) {
            S3VersionSummary vs = versionIter.next();
            s3client.deleteVersion(bucketName, vs.getKey(), vs.getVersionId());
        }
		
        return new ResponseBuckerDto("Successfully deleted",  key); 
    }    
    
	@Override
	public byte[] downloadFile(String key) throws IOException {
	       GetObjectRequest getObjectRequest = new GetObjectRequest(bucketName, key);

	       S3Object s3Object = s3client.getObject(getObjectRequest);
	       S3ObjectInputStream objectInputStream = s3Object.getObjectContent();
	       byte[] bytes = IOUtils.toByteArray(objectInputStream);

	       return bytes;
	}

	@Override
	public List<S3ObjectSummary> list() {
        ObjectListing objectListing = s3client.listObjects(new ListObjectsRequest().withBucketName(bucketName));

        List<S3ObjectSummary> s3ObjectSummaries = objectListing.getObjectSummaries();

        return s3ObjectSummaries;
    }    
    
    private File convertMultiPartToFile(MultipartFile file) throws IOException {
    	File convFile = new File(file.getOriginalFilename());
    	FileOutputStream fos = new FileOutputStream(convFile);
    	fos.write(file.getBytes());
    	fos.close();
    	return convFile;
    }
    
    private String uploadFileTos3bucket(String fileName, File file) {
    	try {
    		s3client.putObject(new PutObjectRequest(bucketName, fileName, file));
    	}catch(AmazonServiceException e) {
    		return "uploadFileTos3bucket().Uploading failed :" + e.getMessage(); 
    	}
    	return "Uploading Successfull";
    }
    
    private boolean existsFile(String key) {
        try {
        	s3client.getObjectMetadata(bucketName,key); 
        } catch(AmazonServiceException e) {
            return false;
        }
        return true;
    }
}
