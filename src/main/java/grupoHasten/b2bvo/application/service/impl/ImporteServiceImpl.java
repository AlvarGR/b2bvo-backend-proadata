package grupoHasten.b2bvo.application.service.impl;

import org.springframework.stereotype.Service;

import grupoHasten.b2bvo.application.dtos.ImporteDto;
import grupoHasten.b2bvo.application.service.ImporteService;

@Service
public class ImporteServiceImpl implements ImporteService {

	@Override
	public ImporteDto calcularImporte(ImporteDto importe) {
		ImporteDto importeDtoCalculate = new ImporteDto();
		
		importeDtoCalculate.setPrecioBase(importe.getPrecioBase());
		
		if (importe.getDescuentoCantidad() != null) {
			importeDtoCalculate.setDescuentoCantidad(importe.getDescuentoCantidad());
			Double porcentaje = importeDtoCalculate.getDescuentoCantidad() / importeDtoCalculate.getPrecioBase() * 100.0;
			Double importeFinal = importeDtoCalculate.getPrecioBase() - importeDtoCalculate.getDescuentoCantidad();
			importeDtoCalculate.setDescuentoPorcentaje(porcentaje);
			importeDtoCalculate.setPrecioFinal(importeFinal);
		}
		
		if (importe.getDescuentoPorcentaje() != null) {
			importeDtoCalculate.setDescuentoPorcentaje(importe.getDescuentoPorcentaje());
			Double cantidad = importeDtoCalculate.getPrecioBase() * importeDtoCalculate.getDescuentoPorcentaje() / 100.0;
			Double importeFinal = importeDtoCalculate.getPrecioBase() - cantidad;
			importeDtoCalculate.setDescuentoCantidad(cantidad);
			importeDtoCalculate.setPrecioFinal(importeFinal);
		}
		
		return importeDtoCalculate;
	}

}
