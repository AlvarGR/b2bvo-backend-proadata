package grupoHasten.b2bvo.application.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import grupoHasten.b2bvo.application.dtos.LoteDto;
import grupoHasten.b2bvo.application.dtos.VehicleDto;
import grupoHasten.b2bvo.application.dtos.convert.LoteConvert;
import grupoHasten.b2bvo.application.dtos.convert.VehicleConvert;
import grupoHasten.b2bvo.application.model.ApplicationUser;
import grupoHasten.b2bvo.application.model.Lote;
import grupoHasten.b2bvo.application.model.Vehicle;
import grupoHasten.b2bvo.application.repository.IUserRepository;
import grupoHasten.b2bvo.application.repository.LoteRepository;
import grupoHasten.b2bvo.application.repository.VehicleRepository;
import grupoHasten.b2bvo.application.service.FavoritesService;

@Service
public class FavoritesServiceImpl implements FavoritesService {

    @Autowired
    IUserRepository userRepository;

    @Autowired
    VehicleRepository vehicleRepository;

    @Autowired
    LoteRepository loteRepository;

    @Autowired
    VehicleConvert vehicleConvert;

    @Autowired
    LoteConvert loteConvert;

    public ApplicationUser addFavoriteVehicle(Long idUsuario, Long idVehicle) {
	Optional<ApplicationUser> optUsuario = userRepository.findById(idUsuario);
	Optional<Vehicle> vehicle = vehicleRepository.findById(idVehicle);
	ApplicationUser usuario = optUsuario.get();
	usuario.addVehicle(vehicle.get());
	userRepository.save(usuario);
	return usuario;
    }

    public ApplicationUser addFavoriteLot(Long idUser, Long idLote) {
	Optional<ApplicationUser> optUusuario = userRepository.findById(idUser);
	ApplicationUser usuario = optUusuario.get();
	Optional<Lote> Lote = loteRepository.findById(idLote);
	usuario.addLot(Lote.get());
	userRepository.save(usuario);
	return usuario;
    }

    public List<VehicleDto> lstFavoritesVehicle(Long idUser) {
	Optional<ApplicationUser> optUusuario = userRepository.findById(idUser);
	ApplicationUser usuario = optUusuario.get();
	List<Vehicle> lstVehicles = new ArrayList<Vehicle>(usuario.getFavoritesVehicle());
	List<VehicleDto> vehiclesDto = new ArrayList<>();
	for (Vehicle item : lstVehicles) {
	    vehiclesDto.add(vehicleConvert.transformEntityToDto(item));
	}
	return vehiclesDto;
    }

    public List<LoteDto> lstFavoritesLotes(Long idUser) {
	Optional<ApplicationUser> optUusuario = userRepository.findById(idUser);
	ApplicationUser usuario = optUusuario.get();
	List<Lote> lstLote = new ArrayList<Lote>(usuario.getFavoritesLot());
	List<LoteDto> lstLoteDto = new ArrayList<LoteDto>();
	for (Lote item : lstLote) {
	    lstLoteDto.add(loteConvert.transformEntityToDto(item));
	}
	return lstLoteDto;
    }

    public List<VehicleDto> removeFavoriteVehicle(Long idUser, Long idVehicle) {
	Optional<ApplicationUser> optUusuario = userRepository.findById(idUser);
	ApplicationUser usuario = optUusuario.get();
	usuario.removeVehicle(vehicleRepository.findById(idVehicle).get());
	userRepository.save(usuario);
	List<Vehicle> lstVehicles = new ArrayList<Vehicle>(usuario.getFavoritesVehicle());
	List<VehicleDto> vehiclesDto = new ArrayList<>();
	for (Vehicle item : lstVehicles) {
	    vehiclesDto.add(vehicleConvert.transformEntityToDto(item));
	}
	return vehiclesDto;
    }

    public List<LoteDto> removeFavoriteLote(Long idUser, Long idLote) {
	Optional<ApplicationUser> optUusuario = userRepository.findById(idUser);
	ApplicationUser usuario = optUusuario.get();
	usuario.removeLot(loteRepository.findById(idLote).get());
	userRepository.save(usuario);
	List<Lote> lstLots = new ArrayList<Lote>(usuario.getFavoritesLot());
	List<LoteDto> lstLoteDto = new ArrayList<LoteDto>();
	for (Lote item : lstLots) {
	    lstLoteDto.add(loteConvert.transformEntityToDto(item));
	}
	return lstLoteDto;
    }

    public boolean consultaVehiculoFavorito(Long idUser, Long idVehicle) {
	Optional<ApplicationUser> optUusuario = userRepository.findById(idUser);
	ApplicationUser usuario = optUusuario.get();
	List<Vehicle> lstVehicles = new ArrayList<Vehicle>(usuario.getFavoritesVehicle());
	try {
	    Optional<Vehicle> comparated = vehicleRepository.findById(idVehicle);
	    if (comparated.isPresent()) {
		Vehicle aux = comparated.get();
		// puede ocurrir la casualidad que de igual cuando no son iguales
		for (Vehicle coche : lstVehicles) {
		    if (aux.getId().equals(coche.getId())) {
			return true;
		    }
		}
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return false;
    }

    public boolean consultaLoteFavorito(Long idUser, Long idLote) {
	Optional<ApplicationUser> optUusuario = userRepository.findById(idUser);
	ApplicationUser usuario = optUusuario.get();
	List<Lote> lstLotes = new ArrayList<Lote>(usuario.getFavoritesLot());
	Optional<Lote> comparated = loteRepository.findById(idLote);
	if (comparated.isPresent()) {
	    Lote aux = comparated.get();
	    for (Lote lote : lstLotes) {
		if (aux.getId().equals(lote.getId())) {
		    return true;
		}
	    }
	}
	return false;
    }
}
