package grupoHasten.b2bvo.application.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import grupoHasten.b2bvo.application.dtos.TransportistaDto;
import grupoHasten.b2bvo.application.dtos.convert.TransportistaConvert;
import grupoHasten.b2bvo.application.model.Transportista;
import grupoHasten.b2bvo.application.repository.TransportistaRepository;
import grupoHasten.b2bvo.application.service.TransportistaService;

@Service
public class TransportistaServiceImpl implements TransportistaService{
    
    @Autowired
    TransportistaRepository transRepository;
    
    @Autowired
    TransportistaConvert transportistaConvert;
    
    public Transportista getTransportista(Long id) {
	return transRepository.getOne(id);
    }

    @Override
    public Transportista createTransportista(TransportistaDto transportista) {
	return transRepository.save(transportistaConvert.convertDtoToEntity(transportista));
    }

    
    

}
