package grupoHasten.b2bvo.application.service.impl;

import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import grupoHasten.b2bvo.application.dtos.NegotiationDto;
import grupoHasten.b2bvo.application.dtos.NegotiationMessageDto;
import grupoHasten.b2bvo.application.dtos.convert.NegotiationConvert;
import grupoHasten.b2bvo.application.dtos.convert.NegotiationMessageConvert;
import grupoHasten.b2bvo.application.dtos.convert.OrganizationConvert;
import grupoHasten.b2bvo.application.enumerates.NegotiationState;
import grupoHasten.b2bvo.application.model.ApplicationUser;
import grupoHasten.b2bvo.application.model.Negotiation;
import grupoHasten.b2bvo.application.model.NegotiationMessage;
import grupoHasten.b2bvo.application.model.Organization;
import grupoHasten.b2bvo.application.model.Vehicle;
import grupoHasten.b2bvo.application.repository.NegotiationRepository;
import grupoHasten.b2bvo.application.service.NegotiationService;
import grupoHasten.b2bvo.application.service.OrganizationService;
import grupoHasten.b2bvo.application.service.TransportistaService;
import grupoHasten.b2bvo.application.service.UserService;
import grupoHasten.b2bvo.application.service.VehicleService;

@Service
public class NegotiationServiceImpl implements  NegotiationService{
    
    @Autowired
    NegotiationConvert negotiationConvert;
    
    @Autowired
    NegotiationRepository negotiationRepository;
    
    @Autowired
    NegotiationMessageConvert messageConverter;
    
    @Autowired
    OrganizationConvert organizationConverter;
    
    @Autowired
    OrganizationService organizationService;
    
    @Autowired
    TransportistaService transportistaService;
    
    @Autowired
    UserService userService;
    
    @Autowired
    VehicleService vehicleService;
    
    @Override
    public NegotiationDto createNegotiationVehicle(Long idComprador, Long idVehiculo,Long idTransportista, String mensaje) throws Exception {

	
	Optional<ApplicationUser> usuarioComprador = null;
	Optional<ApplicationUser> usuarioVendedor = null;
	try {
	    Vehicle vehicle = vehicleService.getVehicleEntity(idVehiculo);
	    
	    usuarioComprador = userService.findUserById(idComprador);
	    usuarioVendedor = userService.findUserById(vehicle.getUploader().getId());
	} catch (Exception e) {
	    e.printStackTrace();
	}
	
	ApplicationUser comprador = usuarioComprador.get();
	ApplicationUser vendedor = usuarioVendedor.get();
	
	Organization organizationComprador = comprador.getOrganizations().iterator().next();
	Organization organizationVendedor = vendedor.getOrganizations().iterator().next();
	
	Negotiation negotiation = new Negotiation();
	NegotiationMessage message = new  NegotiationMessage();
	
	negotiation.setContactoComprador(comprador);
	negotiation.setContactoVendedor(vendedor);
	negotiation.setComprador(organizationService.obtenerOrganizacion(organizationComprador.getId()));
	negotiation.setVendedor(organizationService.obtenerOrganizacion(organizationVendedor.getId()));
	negotiation.setFechaInicio(new Date());
	negotiation.setState(NegotiationState.getByKey(0L));
	negotiation.setFechaUltimoMensaje(new Date());
	negotiation.setIdentification(negotiation.getIdentification());
	
	if(idTransportista!=null) {
	    negotiation.setTransportista(transportistaService.getTransportista(idTransportista));
	}
	
	message.setFechaEmision(new Date());
	message.setMessage(mensaje);
	message.setSender(comprador);
	
	negotiation.addMessage(message);
	
	Negotiation negotiationSaved = negotiationRepository.save(negotiation);
	
	return negotiationConvert.transformEntityToDto(negotiationSaved);
    }
    
    @Override
    public NegotiationDto createNegotiationLote(Long idComprador, Long idVendedor, Long idLote) {
	// TODO Auto-generated method stub
	return null;
    }
    
    public NegotiationDto getNegotiation(Long id) {
	
	Negotiation negotiation = negotiationRepository.getOne(id);
	
	return negotiationConvert.transformEntityToDto(negotiation);
    }
    
    @Override
    public NegotiationDto saveMessageNegotiation(NegotiationMessageDto negotiationMessageDto) {
	// TODO Auto-generated method stub
	Optional<Negotiation> optSavedNegotiation = negotiationRepository.findById(negotiationMessageDto.getIdNegotiation()); 
	
	Negotiation savedNegotiation = optSavedNegotiation.get();
	
	savedNegotiation.addMessage(messageConverter.TransformDtoToEntity(negotiationMessageDto));
	
	NegotiationDto negotiationResult = negotiationConvert.transformEntityToDto(negotiationRepository.save(savedNegotiation));	
	
	return negotiationResult;
    }

    public NegotiationState getNegotiationState(Long key) {
	return NegotiationState.getByKey(key);
    }

//    @Override
//    public NegotiationDto createNegotiationVehicle(Long idComprador, Long idVendedor, Long idVehiculo) {
//	// TODO Auto-generated method stub
//	return null;
//    }



}
