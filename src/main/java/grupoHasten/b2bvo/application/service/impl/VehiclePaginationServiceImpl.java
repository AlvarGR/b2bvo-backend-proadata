package grupoHasten.b2bvo.application.service.impl;

import java.util.function.Function;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import grupoHasten.b2bvo.application.dtos.SearchCriteriaDto;
import grupoHasten.b2bvo.application.dtos.VehicleDto;
import grupoHasten.b2bvo.application.dtos.convert.VehicleConvert;
import grupoHasten.b2bvo.application.model.Vehicle;
import grupoHasten.b2bvo.application.repository.VehiclePaginationRepository;
import grupoHasten.b2bvo.application.repository.daos.VehicleDao;
import grupoHasten.b2bvo.application.service.VehiclePaginationService;

@Service
public class VehiclePaginationServiceImpl implements VehiclePaginationService {

	@Autowired
	VehiclePaginationRepository vehiclePaginationRepository;
	
	@Autowired
	VehicleConvert vehicleConvert;
	
	@Autowired
	VehicleDao vehicleDao;

    @Value("${paginacion.numObjectForPage}")
    private int numObjectForPage;

	@Override
	public Page<VehicleDto> getVehicles(Long page) {
		int numPage = page.intValue();
		Pageable pageable =  PageRequest.of(numPage, numObjectForPage);
		
		Page<Vehicle> pageEntities = vehiclePaginationRepository.findAll(pageable);
		Page<VehicleDto> dtoPage = pageEntities.map(new Function<Vehicle, VehicleDto>() {
		    public VehicleDto apply(Vehicle entity) {
		        return vehicleConvert.transformEntityToDto(entity);
		    }
		});
		
		return dtoPage;
	}

	@Override
	public Page<VehicleDto> getVehiclesFilter(Long page, SearchCriteriaDto filters) {
		
		
		Page<Vehicle> pageEntities =searchVehicles(page, filters);
		Page<VehicleDto> dtoPage = pageEntities.map(new Function<Vehicle, VehicleDto>() {
		    public VehicleDto apply(Vehicle entity) {
		        return vehicleConvert.transformEntityToDto(entity);
		    }
		});
		return dtoPage;
	}
	
	@Override
	public Page<Vehicle> searchVehicles (Long page, SearchCriteriaDto filters){
	  int numPage = page.intValue();
      Pageable pageable =  PageRequest.of(numPage, numObjectForPage);
	  return vehicleDao.getAllVehicles(pageable, filters);
	}

}
