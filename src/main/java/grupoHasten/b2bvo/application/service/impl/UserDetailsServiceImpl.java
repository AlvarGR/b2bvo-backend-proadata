package grupoHasten.b2bvo.application.service.impl;

import static java.util.Collections.emptyList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import grupoHasten.b2bvo.application.repository.IUserRepository;

@Service
@Transactional
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private IUserRepository applicationUserRepository;

    public UserDetailsServiceImpl(IUserRepository applicationUserRepository) {
	this.applicationUserRepository = applicationUserRepository;
    }

    @Override
    public User loadUserByUsername(String username) throws UsernameNotFoundException {

	grupoHasten.b2bvo.application.model.ApplicationUser applicationUser = new grupoHasten.b2bvo.application.model.ApplicationUser();
	applicationUser = null;
	try {
	    applicationUser = applicationUserRepository.findByName(username);
	    // .orElseThrow(() -> new UsernameNotFoundException("ApplicationUser does not exxist!"));
	} catch (Exception e) {
	    e.printStackTrace();
	}
	if (applicationUser == null) {
	    throw new UsernameNotFoundException(username);
	}
//     grupoHasten.b2bvo.application.model.ApplicationUser usuario = applicationUser.get();
//      Set grantList = new HashSet();
// 
//      for(Role role:usuario.getRoles()) {
//	  GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(role.getUserRoles().toString());
//          grantList.add(grantedAuthority);
//      }

	// ApplicationUser usuario = applicationUser.get();

	return new User(applicationUser.getName(), applicationUser.getPassword(), emptyList());
    }
}
