package grupoHasten.b2bvo.application.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import grupoHasten.b2bvo.application.dtos.LoteDto;
import grupoHasten.b2bvo.application.dtos.VehicleDto;
import grupoHasten.b2bvo.application.dtos.convert.LoteConvert;
import grupoHasten.b2bvo.application.model.Lote;
import grupoHasten.b2bvo.application.model.Vehicle;
import grupoHasten.b2bvo.application.repository.LoteRepository;
import grupoHasten.b2bvo.application.repository.VehicleRepository;
import grupoHasten.b2bvo.application.service.LoteService;
import grupoHasten.b2bvo.application.service.UserService;
import grupoHasten.b2bvo.application.service.VehiclePaginationService;
import grupoHasten.b2bvo.application.service.exception.LoteNotFoundException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class LoteServiceImpl implements LoteService {

  @Value("${paginacion.numObjectForPage}")
  private int numObjectForPage;

  @Autowired
  LoteConvert loteConvert;

  @Autowired
  LoteRepository loteRepository;

  @Autowired
  UserService userService;

  @Autowired
  VehicleRepository vehicleRepository;

  @Autowired
  VehiclePaginationService vehiclePaginationService;

  @Override
  public LoteDto saveLote(LoteDto loteDto) {
    Lote lote = loteConvert.transformDtoToEntity(loteDto);

    lote.setVehicles(buildVehicles(loteDto.getVehiclesDto()));

    lote.setUploader(userService.findUserById(loteDto.getUploader()).get());

    Lote newLote = loteRepository.save(lote);

    for (Vehicle item : lote.getVehicles()) {
      Vehicle vehicle = vehicleRepository.findById(item.getId()).get();
      vehicle.setLote(newLote);
      vehicleRepository.save(vehicle);
    }
    return loteConvert.transformEntityToDto(newLote);
  }


  @Override
  public LoteDto obtenerLote(Long id) throws LoteNotFoundException {
    Lote lote = loteRepository.findById(id).orElseThrow(
        () -> new LoteNotFoundException(String.format("Lote with id = %d Not Found", id)));
    return loteConvert.transformEntityToDto(lote);
  }

  @Override
  public Page<LoteDto> searchLote(String make, String model, String version,
      Date registrationDateFrom, Date registrationDateTo, Double basePriceFrom, Double basePriceTo,
      String fuel, Long kmFrom, Long kmTo, String numberOfDoorsFrom, String numberOfDoorsTo,
      Long seatingCapacityFrom, Long seatingCapacityTo, int pageIndex) {
    Page<Lote> lotes = null;
    Pageable pageable = PageRequest.of(pageIndex, numObjectForPage);
    if (StringUtils.isAllBlank(make, model, fuel, numberOfDoorsTo, numberOfDoorsFrom)
        && registrationDateFrom == null && registrationDateTo == null && kmFrom == null
        && kmTo == null && seatingCapacityFrom == null && seatingCapacityTo == null
        && basePriceTo == null && basePriceFrom == null) {
      lotes = loteRepository.findAll(pageable);
    } else {
      lotes = loteRepository.searchLote(make, model, version, registrationDateFrom,
          registrationDateTo, basePriceFrom, basePriceTo, fuel, kmFrom, kmTo, numberOfDoorsFrom,
          numberOfDoorsTo, seatingCapacityFrom, seatingCapacityTo, pageable);
    }

    return loteConvert.transformEntityToDtoLst(lotes);
  }

  private List<Vehicle> buildVehicles(List<VehicleDto> vehiclesDto) {
    List<Vehicle> vehicles = new ArrayList<>();
    for (VehicleDto vehicleDto : vehiclesDto) {
      Vehicle vehicle = vehicleRepository.findById(vehicleDto.getId()).get();
      vehicles.add(vehicle);
    }
    return vehicles;
  }


  @Override
  public List<LoteDto> obtenerLotes() {
    List<Lote> lotes = loteRepository.findAll();
    List<LoteDto> lotesDto = new ArrayList<>();
    for (Lote item : lotes) {
      lotesDto.add(loteConvert.transformEntityToDto(item));
    }
    return lotesDto;
  }


  @Override
  public LoteDto actualizarLote(Long id, LoteDto loteDto) throws LoteNotFoundException {
    Lote lote = loteRepository.findById(id).get();
    Lote loteToUpdate = loteConvert.actualizarEntity(lote, loteDto);
    lote.getVehicles().clear();
    loteToUpdate.getVehicles().addAll(buildListVehicles(loteDto));
    Lote newLote = loteRepository.save(loteToUpdate);

    for (Vehicle item : newLote.getVehicles()) {
      Vehicle vehicle = vehicleRepository.findById(item.getId()).get();
      vehicle.setLote(newLote);
      vehicleRepository.save(vehicle);
    }
    return loteConvert.transformEntityToDto(newLote);
  }



  private List<Vehicle> buildListVehicles(LoteDto loteDto) {
    List<Vehicle> newListVehicles = new ArrayList<>();
    for (VehicleDto vehicleDto : loteDto.getVehiclesDto()) {
      newListVehicles.add(vehicleRepository.findById(vehicleDto.getId()).get());
    }

    return newListVehicles;
  }



}
