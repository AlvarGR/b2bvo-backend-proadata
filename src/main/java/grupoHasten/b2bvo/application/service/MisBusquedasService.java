package grupoHasten.b2bvo.application.service;

import java.util.List;

import grupoHasten.b2bvo.application.dtos.MisBusquedasDto;

public interface MisBusquedasService {

    List<MisBusquedasDto> getMisBusquedasAll(String user);

    MisBusquedasDto getMisBusquedas(Long id);

    MisBusquedasDto saveMisBusquedas(MisBusquedasDto misBusquedasDto);

    void removeMisBusquedas(Long id);

}
