package grupoHasten.b2bvo.application.service;

import grupoHasten.b2bvo.application.model.Organization;

public interface OrganizationService {
    
    Organization obtenerOrganizacion(Long id);

}
