package grupoHasten.b2bvo.application.service;

import org.springframework.data.domain.Page;

import grupoHasten.b2bvo.application.dtos.SearchCriteriaDto;
import grupoHasten.b2bvo.application.dtos.VehicleDto;
import grupoHasten.b2bvo.application.model.Vehicle;

public interface VehiclePaginationService {

    Page<VehicleDto> getVehicles(Long page);

    Page<VehicleDto> getVehiclesFilter(Long page, SearchCriteriaDto filters);

    Page<Vehicle> searchVehicles(Long page, SearchCriteriaDto filters);
}
