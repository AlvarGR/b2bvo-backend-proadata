package grupoHasten.b2bvo.application.service;

import grupoHasten.b2bvo.application.dtos.ImporteDto;

public interface ImporteService {
    
    ImporteDto calcularImporte(ImporteDto importe);
}
