package grupoHasten.b2bvo.application.service;

import java.util.List;
import java.util.Optional;

import grupoHasten.b2bvo.application.model.ApplicationUser;

public interface UserService {

    public ApplicationUser existUser(String nameOrEmail, String password);
    
    public List<ApplicationUser> listarUsuarios();
    
    public ApplicationUser saveUser(ApplicationUser usuario);
    
    public Optional<ApplicationUser> findUserById(Long id);
    
    public ApplicationUser findUserByResetToken(String resetToken);
    
    public ApplicationUser findUserByEmail(String userEmail);
    
    public void createPasswordResetTokenForUser(ApplicationUser user, String token);
    
}
