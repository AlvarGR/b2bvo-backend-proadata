package grupoHasten.b2bvo.application.service;

import org.springframework.data.domain.Page;

import grupoHasten.b2bvo.application.dtos.LoteDto;
import grupoHasten.b2bvo.application.service.exception.LoteNotFoundException;

import java.util.Date;
import java.util.List;

public interface LoteService {
    
	LoteDto saveLote(LoteDto loteDto);
	
	List<LoteDto> obtenerLotes();
	
	LoteDto obtenerLote(Long id)  throws LoteNotFoundException;
	
	LoteDto actualizarLote(Long id, LoteDto loteDto) throws LoteNotFoundException;

  Page<LoteDto> searchLote(String make, String model, String version, Date registrationDateFrom,
      Date registrationDateTo, Double basePriceFrom, Double basePriceTo, String fuel, Long kmFrom,
      Long kmTo, String numberOfDoorsFrom, String numberOfDoorsTo, Long seatingCapacityFrom,
      Long seatingCapacityTo, int pageIndex);
}
