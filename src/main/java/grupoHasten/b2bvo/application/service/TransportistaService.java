package grupoHasten.b2bvo.application.service;

import grupoHasten.b2bvo.application.dtos.TransportistaDto;
import grupoHasten.b2bvo.application.model.Transportista;

public interface TransportistaService {

    public Transportista getTransportista(Long id);

    public Transportista createTransportista(TransportistaDto transportista);

}
