package grupoHasten.b2bvo.application.service;

import grupoHasten.b2bvo.application.dtos.NegotiationDto;
import grupoHasten.b2bvo.application.dtos.NegotiationMessageDto;
import grupoHasten.b2bvo.application.service.exception.VehicleNotFoundException;

public interface NegotiationService {

    public NegotiationDto createNegotiationVehicle(Long idComprador, Long idVehiculo,Long idTransportista, String mensaje)throws Exception;
    
    public NegotiationDto createNegotiationLote(Long idComprador, Long idVendedor,Long idLote);

    public NegotiationDto getNegotiation(Long id);

    public NegotiationDto saveMessageNegotiation(NegotiationMessageDto negotiationMessageDto);
    
}
