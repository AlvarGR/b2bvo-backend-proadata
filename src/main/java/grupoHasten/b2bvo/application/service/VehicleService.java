package grupoHasten.b2bvo.application.service;

import java.util.List;

import org.springframework.data.domain.Page;

import grupoHasten.b2bvo.application.dtos.VehicleDto;
import grupoHasten.b2bvo.application.model.Vehicle;
import grupoHasten.b2bvo.application.service.exception.VehicleNotFoundException;

public interface VehicleService {

  VehicleDto saveVehicle(VehicleDto vehicleDto);

  List<VehicleDto> getVehicles();


  Page<VehicleDto> findVehicleByUserId(int pageIndex, Long userId);

    VehicleDto getVehicle(Long id) throws VehicleNotFoundException;

    Vehicle getVehicleEntity(Long id) throws VehicleNotFoundException;
}
