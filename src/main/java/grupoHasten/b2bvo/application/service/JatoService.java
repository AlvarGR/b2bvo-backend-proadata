package grupoHasten.b2bvo.application.service;

import java.util.List;

import grupoHasten.b2bvo.application.facade.model.JatoFilter;
import grupoHasten.b2bvo.application.facade.model.JatoMake;
import grupoHasten.b2bvo.application.facade.model.JatoMakeModel;
import grupoHasten.b2bvo.application.facade.model.JatoModel;
import grupoHasten.b2bvo.application.facade.model.JatoVersion;

public interface JatoService {
    
    List<JatoMake> obtenerMakes();

    List<JatoModel> obtenerModel(String make);

    JatoFilter obtenerFilter(String make, String model);

    List<JatoVersion> obtenerVersion(String make, String model);

    List<JatoMakeModel> obtenerJatoMakeModel();

}
