package grupoHasten.b2bvo.application.service;

import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.services.s3.model.S3ObjectSummary;

import grupoHasten.b2bvo.application.dtos.ResponseBuckerDto;

public interface AmazonClient {

    public ResponseBuckerDto uploadFile(MultipartFile multipartFile) throws IOException;

    public byte[] downloadFile(String key) throws IOException;

    public List<S3ObjectSummary> list();

    public ResponseBuckerDto deleteFileFromS3Bucket(String key);
}
