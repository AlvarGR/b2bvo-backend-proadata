package grupoHasten.b2bvo.application.service.exception;

@SuppressWarnings("serial")
public class ApplicationUserNotFoundException extends Exception {

    public ApplicationUserNotFoundException(String errorMessage) {
	super(errorMessage);
    }
}
