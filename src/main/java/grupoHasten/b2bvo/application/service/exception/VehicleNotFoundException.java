package grupoHasten.b2bvo.application.service.exception;

@SuppressWarnings("serial")
public class VehicleNotFoundException extends Exception {
	
    public VehicleNotFoundException(String errorMessage) {
        super(errorMessage);
    }
}
