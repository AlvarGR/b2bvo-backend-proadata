package grupoHasten.b2bvo.application.service.exception;

@SuppressWarnings("serial")
public class LoteNotFoundException extends Exception {

	public LoteNotFoundException(String errorMessage) {
        super(errorMessage);
	}
}
