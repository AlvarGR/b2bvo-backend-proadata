package grupoHasten.b2bvo.application.service;

import java.util.List;

import grupoHasten.b2bvo.application.dtos.LoteDto;
import grupoHasten.b2bvo.application.dtos.VehicleDto;
import grupoHasten.b2bvo.application.model.ApplicationUser;

public interface FavoritesService {

    ApplicationUser addFavoriteVehicle(Long idUsuario, Long idVehicle);

    ApplicationUser addFavoriteLot(Long idUser, Long idLote);

    List<VehicleDto> lstFavoritesVehicle(Long idUser);

    List<LoteDto> lstFavoritesLotes(Long idUser);

    List<VehicleDto> removeFavoriteVehicle(Long idUser, Long idVehicle);

    List<LoteDto> removeFavoriteLote(Long idUser, Long idLote);

    boolean consultaVehiculoFavorito(Long idUser, Long idVehicle);

    boolean consultaLoteFavorito(Long idUser, Long idLote);

}
