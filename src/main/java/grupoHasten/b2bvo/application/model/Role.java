package grupoHasten.b2bvo.application.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "role")
public class Role implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

//    @Enumerated(EnumType.STRING)
//    @NaturalId
//    @Column(length = 60, unique = true, nullable = false)
//    private RoleName name;

//    @JsonIgnore
//    @ManyToMany(mappedBy = "roles")
//    private Set<ApplicationUser> userRoles;

    public Role() {

    }

//    public Role(RoleName name) {
//	this.name = name;
//    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

//    public RoleName getName() {
//	return name;
//    }
//
//    public void setName(RoleName name) {
//	this.name = name;
//    }

//    public Set<ApplicationUser> getUserRoles() {
//	return userRoles;
//    }
//
//    public void setUserRoles(Set<ApplicationUser> userRoles) {
//	this.userRoles = userRoles;
//    }

}