package grupoHasten.b2bvo.application.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import grupoHasten.b2bvo.application.enumerates.VehicleState;

@SuppressWarnings("serial")
@Entity
@Table(name = "vehicle")
public class Vehicle implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "seller_id")
    private Organization seller;

    @OneToOne
    @JoinColumn(name = "engine_id")
    private Engine engine;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "lote_id")
    private Lote lote;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "vehicle_id")
    private List<Attachment> attachments = new ArrayList<>();

    @Column
    private Date registrationDate;

    @Column
    private Date availabilityDate;

    @Column
    private String make;

    @Column
    private String models;

    @Column
    private String version;

    @Column
    private String plateNumber;

    @Column
    private Long milleage;

    @Column
    private Double basePrice;

    @Column
    private Double discountPercentage;

    @Column
    private Double discountAmount;

    @Column
    private Double finalPrice;

    @Column(name = "state")
    @Enumerated(value = EnumType.STRING)
    private VehicleState vehicleState;

    @Column(columnDefinition = "TEXT")
    private String comments;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "type_id")
    private VehicleType vehicleType;

    @Column
    private Boolean visible;

    @OneToOne
    @JoinColumn(name = "uploader_id")
    private ApplicationUser uploader;
    
    public void addAttachment(Attachment attachment) {
	if (attachments == null) {
	    attachments = new ArrayList<>();
	}
	attachments.add(attachment);
    }

    public void deleteAttachment(Attachment attachment) {
	attachments.remove(attachment);
    }

    public List<Attachment> getAttachments() {
	return attachments;
    }

    public void setAttachments(List<Attachment> attachments) {
	this.attachments = attachments;
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public Date getRegistrationDate() {
	return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
	this.registrationDate = registrationDate;
    }

    public String getPlateNumber() {
	return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
	this.plateNumber = plateNumber;
    }

    public Long getMilleage() {
	return milleage;
    }

    public void setMilleage(Long milleage) {
	this.milleage = milleage;
    }

    public VehicleType getVehicleType() {
	return vehicleType;
    }

    public void setVehicleType(VehicleType vehicleType) {
	this.vehicleType = vehicleType;
    }

    public Boolean getVisible() {
	return visible;
    }

    public void setVisible(Boolean visible) {
	this.visible = visible;
    }

    public ApplicationUser getUploader() {
        return uploader;
    }

    public void setUploader(ApplicationUser uploader) {
        this.uploader = uploader;
    }

    public Engine getEngine() {
	return engine;
    }

    public void setEngine(Engine engine) {
	this.engine = engine;
    }

    public Lote getLote() {
	return lote;
    }

    public void setLote(Lote lote) {
	this.lote = lote;
    }

    public Organization getSeller() {
	return seller;
    }

    public void setSeller(Organization seller) {
	this.seller = seller;
    }

    public String getMake() {
	return make;
    }

    public void setMake(String make) {
	this.make = make;
    }

    public String getModels() {
	return models;
    }

    public void setModels(String models) {
	this.models = models;
    }

    public String getVersion() {
	return version;
    }

    public void setVersion(String version) {
	this.version = version;
    }

    public String getComments() {
	return comments;
    }

    public void setComments(String comments) {
	this.comments = comments;
    }

    public Date getAvailabilityDate() {
	return availabilityDate;
    }

    public void setAvailabilityDate(Date availabilityDate) {
	this.availabilityDate = availabilityDate;
    }

    public VehicleState getVehicleState() {
	return vehicleState;
    }

    public void setVehicleState(VehicleState vehicleState) {
	this.vehicleState = vehicleState;
    }

    public Double getBasePrice() {
	return basePrice;
    }

    public void setBasePrice(Double basePrice) {
	this.basePrice = basePrice;
    }

    public Double getDiscountPercentage() {
	return discountPercentage;
    }

    public void setDiscountPercentage(Double discountPercentage) {
	this.discountPercentage = discountPercentage;
    }

    public Double getDiscountAmount() {
	return discountAmount;
    }

    public void setDiscountAmount(Double discountAmount) {
	this.discountAmount = discountAmount;
    }

    public Double getFinalPrice() {
	return finalPrice;
    }

    public void setFinalPrice(Double finalPrice) {
	this.finalPrice = finalPrice;
    }

    @Override
    public int hashCode() {
	return Objects.hash(attachments, availabilityDate, basePrice, comments, discountAmount, discountPercentage,
		engine, finalPrice, id, lote, make, milleage, models, plateNumber, registrationDate, seller,
		vehicleState, vehicleType, version, visible);
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (!(obj instanceof Vehicle))
	    return false;
	Vehicle other = (Vehicle) obj;
	return Objects.equals(attachments, other.attachments)
		&& Objects.equals(availabilityDate, other.availabilityDate)
		&& Objects.equals(basePrice, other.basePrice) && Objects.equals(comments, other.comments)
		&& Objects.equals(discountAmount, other.discountAmount)
		&& Objects.equals(discountPercentage, other.discountPercentage) && Objects.equals(engine, other.engine)
		&& Objects.equals(finalPrice, other.finalPrice) && Objects.equals(id, other.id)
		&& Objects.equals(lote, other.lote) && Objects.equals(make, other.make)
		&& Objects.equals(milleage, other.milleage) && Objects.equals(models, other.models)
		&& Objects.equals(plateNumber, other.plateNumber)
		&& Objects.equals(registrationDate, other.registrationDate) && Objects.equals(seller, other.seller)
		&& vehicleState == other.vehicleState && Objects.equals(vehicleType, other.vehicleType)
		&& Objects.equals(version, other.version) && Objects.equals(visible, other.visible);
    }

}
