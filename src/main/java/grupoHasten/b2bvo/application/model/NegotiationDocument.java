package grupoHasten.b2bvo.application.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import grupoHasten.b2bvo.application.enumerates.DocumentClass;

@SuppressWarnings("serial")
@Entity
@Table(name="NegotiationDocument")
public class NegotiationDocument implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "uploader_id", foreignKey = @ForeignKey(name = "fk_negotiationdocument_uploader"))
    private ApplicationUser uploader; 
    
    @Column
    private Date fechaUpload;
    
    @Column(name="clase", length = 50)
    @Enumerated(value = EnumType.STRING)
    private DocumentClass clase;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "attachment_id", foreignKey = @ForeignKey(name = "fk_negotiationdocument_attachment"))
    private Attachment attachment; 
    
    @Column(name="TypeValidation", length = 50)
    @Enumerated(value = EnumType.STRING)
    private DocumentClass validation;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ApplicationUser getUploader() {
		return uploader;
	}

	public void setUploader(ApplicationUser uploader) {
		this.uploader = uploader;
	}

	public Date getFechaUpload() {
		return fechaUpload;
	}

	public void setFechaUpload(Date fechaUpload) {
		this.fechaUpload = fechaUpload;
	}

	public DocumentClass getClase() {
		return clase;
	}

	public void setClase(DocumentClass clase) {
		this.clase = clase;
	}

	public Attachment getAttachment() {
		return attachment;
	}

	public void setAttachment(Attachment attachment) {
		this.attachment = attachment;
	}

	public DocumentClass getValidation() {
		return validation;
	}

	public void setValidation(DocumentClass validation) {
		this.validation = validation;
	}
}
