package grupoHasten.b2bvo.application.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "lote")
public class Lote implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column
    private Double basePrice;
    
    @OneToOne
    @JoinColumn(name = "uploader_id")
    private ApplicationUser uploader;

    @Column
    private Double discountPercentage;

    @Column
    private Double discountAmount;

    @Column
    private Double finalPrice;

    @Column
    private Date publicationDate;

    @Column
    private Date availabilityDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "contactPerson_id")
    private ApplicationUser contactPerson;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "organization_id")
    private Organization organization;
    
    @Column
    private String deliveryPlace;

    @OneToMany(mappedBy = "lote", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Vehicle> vehicles = new ArrayList<>();

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public Date getPublicationDate() {
	return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
	this.publicationDate = publicationDate;
    }

    public String getDeliveryPlace() {
	return deliveryPlace;
    }

    public void setDeliveryPlace(String deliveryPlace) {
	this.deliveryPlace = deliveryPlace;
    }

    public List<Vehicle> getVehicles() {
	return vehicles;
    }

    public void setVehicles(List<Vehicle> vehicles) {
	this.vehicles = vehicles;
    }

    public Double getBasePrice() {
	return basePrice;
    }

    public ApplicationUser getUploader() {
        return uploader;
    }

    public void setUploader(ApplicationUser uploader) {
        this.uploader = uploader;
    }

    public void setBasePrice(Double basePrice) {
	this.basePrice = basePrice;
    }

    public Double getDiscountPercentage() {
	return discountPercentage;
    }

    public void setDiscountPercentage(Double discountPercentage) {
	this.discountPercentage = discountPercentage;
    }

    public Double getDiscountAmount() {
	return discountAmount;
    }

    public void setDiscountAmount(Double discountAmount) {
	this.discountAmount = discountAmount;
    }

    public Double getFinalPrice() {
	return finalPrice;
    }

    public void setFinalPrice(Double finalPrice) {
	this.finalPrice = finalPrice;
    }

    public Date getAvailabilityDate() {
	return availabilityDate;
    }

    public void setAvailabilityDate(Date availabilityDate) {
	this.availabilityDate = availabilityDate;
    }

    public ApplicationUser getContactPerson() {
	return contactPerson;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public void setContactPerson(ApplicationUser contactPerson) {
	this.contactPerson = contactPerson;
    }

}
