package grupoHasten.b2bvo.application.model;

public class LoginRequest {
    
    private String usernameOrEmail;
    private String password;
//    private Claims claims;

    public String getUsernameOrEmail() {
	return usernameOrEmail;
    }

    public void setUsernameOrEmail(String usernameOrEmail) {
	this.usernameOrEmail = usernameOrEmail;
    }

    public String getPassword() {
	return password;
    }

    public void setPassword(String password) {
	this.password = password;
    }
    
//    public Claims getClaims() {
//        return claims;
//    }
//
//    public void setClaims(Claims claims) {
//        this.claims = claims;
//    }
}
