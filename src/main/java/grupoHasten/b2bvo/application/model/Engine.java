package grupoHasten.b2bvo.application.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name="engine")
public class Engine implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    
    private Long engineCC;
    private Long numberOfCylinders;
    private Long maximumPowerKw;
    private Long maximumPowerCv;
    private String litres;
    private String transmissionType;
    private String compressor;
    private String fuelType;
    private Long co2Emission;
    private String numberOfDoors;
    private Long seatingCapacity;
    
	public String getNumberOfDoors() {
		return numberOfDoors;
	}
	public void setNumberOfDoors(String numberOfDoors) {
		this.numberOfDoors = numberOfDoors;
	}
	public Long getSeatingCapacity() {
		return seatingCapacity;
	}
	public void setSeatingCapacity(Long seatingCapacity) {
		this.seatingCapacity = seatingCapacity;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getEngineCC() {
		return engineCC;
	}
	public void setEngineCC(Long engineCC) {
		this.engineCC = engineCC;
	}
	public Long getNumberOfCylinders() {
		return numberOfCylinders;
	}
	public void setNumberOfCylinders(Long numberOfCylinders) {
		this.numberOfCylinders = numberOfCylinders;
	}
	public Long getMaximumPowerKw() {
		return maximumPowerKw;
	}
	public void setMaximumPowerKw(Long maximumPowerKw) {
		this.maximumPowerKw = maximumPowerKw;
	}
	public Long getMaximumPowerCv() {
		return maximumPowerCv;
	}
	public void setMaximumPowerCv(Long maximumPowerCv) {
		this.maximumPowerCv = maximumPowerCv;
	}
	public String getLitres() {
		return litres;
	}
	public void setLitres(String litres) {
		this.litres = litres;
	}
	public String getTransmissionType() {
		return transmissionType;
	}
	public void setTransmissionType(String transmissionType) {
		this.transmissionType = transmissionType;
	}
	public String getCompressor() {
		return compressor;
	}
	public void setCompressor(String compressor) {
		this.compressor = compressor;
	}
	public String getFuelType() {
		return fuelType;
	}
	public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}
	public Long getCo2Emission() {
		return co2Emission;
	}
	public void setCo2Emission(Long co2Emission) {
		this.co2Emission = co2Emission;
	}
}
