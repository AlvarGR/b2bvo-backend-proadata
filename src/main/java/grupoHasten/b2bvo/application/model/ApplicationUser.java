package grupoHasten.b2bvo.application.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "user")
public class ApplicationUser implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_seq")
    @SequenceGenerator(name = "user_seq", allocationSize = 1)
    private Long id;

    @Column(name = "name", length = 50)
    private String name;

    @Column(name = "created_at")
    private Date created_at;

    @Column(name = "updated_at")
    private Date updated_at;

    @Column(name = "enabled")
    private boolean enabled;

    @Column(name = "is_active")
    private boolean active;

    @Column(name = "is_email_verified")
    private boolean emailVerified;

    @Column(name = "position")
    private String position;

    @Column(name = "lastName")
    private String lastName;

    @Column(name = "password")
    private String password;

    @Column(name = "national_id")
    private String national_id;

    @Column(name = "email")
    private String email;

    @Column(name = "is_tos_acepted")
    private boolean is_tos_accepted;

    @Column(name = "reset_token", unique = true)
    private String resetToken;

    @Transient
    private String postalCode;

    @Transient
    private String streetName;

    @Transient
    private String municipality;

    @Transient
    private String country;

    @Transient
    private String taxId;

    @Transient
    private String companyName;

    //-------------------------------Organizaciones, empresas
    
    @JsonIgnore
    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(name = "organization_users", joinColumns = { @JoinColumn(name = "user_id") }, inverseJoinColumns = {
	    @JoinColumn(name = "organization_id") })
    private Set<Organization> organizations = new HashSet<Organization>();

    public void addOrganization(Organization organization) {
	this.organizations.add(organization);
	// address.getOrganizations().add(this);
    }

    public void removeOrganization(Organization organization) {
	this.organizations.remove(organization);
    }

    public void removeOrganizations() {
	Iterator<Organization> iterator = this.organizations.iterator();

	while (iterator.hasNext()) {
	    iterator.remove();
	}
    }
    // -------------------------------favoritos coches

    @JsonIgnore
    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(name = "favoritesUserCar", joinColumns = { @JoinColumn(name = "user_id") }, inverseJoinColumns = {
	    @JoinColumn(name = "vehicle_id") })
    private Set<Vehicle> favoritesVehicle = new HashSet<Vehicle>();

    public void addVehicle(Vehicle vehicle) {
	this.favoritesVehicle.add(vehicle);
    }

    public void removeVehicle(Vehicle vehicle) {
	this.favoritesVehicle.remove(vehicle);
    }

    public void removeVehicles() {
	Iterator<Vehicle> iterator = this.favoritesVehicle.iterator();
	while (iterator.hasNext()) {
	    iterator.remove();
	}
    }

    // -------------------------------favoritos lotes
    @JsonIgnore
    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(name = "favoritesUserlots", joinColumns = { @JoinColumn(name = "user_id") }, inverseJoinColumns = {
	    @JoinColumn(name = "lote_id") })
    private Set<Lote> favoritesLot = new HashSet<Lote>();

    public void addLot(Lote vehicle) {
	this.favoritesLot.add(vehicle);
    }

    public void removeLot(Lote lot) {
	this.favoritesLot.remove(lot);
    }

    public void removeLots() {
	Iterator<Lote> iterator = this.favoritesLot.iterator();
	while (iterator.hasNext()) {
	    iterator.remove();
	}
    }

// private Set<Address> orgAddresses = new HashSet();
//  @Column(name = "is_active")
//  private Boolean String;
//
//  @Column(name = "position")
//  private boolean position;
//  
//  @Column(name = "position")
//  private boolean position;
//  
//  @Column(name = "position")
//  private boolean position;
//    
//  @Column(name = "role")
//  private int role;
//  @Column(name = "avatar", insertable = false, updatable = false)
//  private String avatar;
//
//  @ManyToMany(fetch = FetchType.LAZY)
//  @JoinTable(name = "user_roles", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
//  private Set<Role> roles;

//    public ApplicationUser Removefavorites() {
//	return null;
//    }

//    public ApplicationUser(String name, String password/* , Set<Role> roles */) {
//	this.setName(name);
//	this.setPassword(password);
//	// this.setRoles(roles);
//    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public Date getCreated_at() {
	return created_at;
    }

    public void setCreated_at(Date created_at) {
	this.created_at = created_at;
    }

    public Date getUpdated_at() {
	return updated_at;
    }

    public void setUpdated_at(Date updated_at) {
	this.updated_at = updated_at;
    }

    public boolean isEnabled() {
	return enabled;
    }

    public void setEnabled(boolean enabled) {
	this.enabled = enabled;
    }

    public boolean isActive() {
	return active;
    }

    public void setActive(boolean active) {
	this.active = active;
    }

    public boolean isEmailVerified() {
	return emailVerified;
    }

    public void setEmailVerified(boolean emailVerified) {
	this.emailVerified = emailVerified;
    }

    public String getPosition() {
	return position;
    }

    public void setPosition(String position) {
	this.position = position;
    }

    public String getLastName() {
	return lastName;
    }

    public void setLastName(String lastName) {
	this.lastName = lastName;
    }

    public String getPassword() {
	return password;
    }

    public void setPassword(String password) {
	this.password = password;
    }

    public String getNational_id() {
	return national_id;
    }

    public void setNational_id(String national_id) {
	this.national_id = national_id;
    }

    public String getEmail() {
	return email;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    public boolean isIs_tos_accepted() {
	return is_tos_accepted;
    }

    public void setIs_tos_accepted(boolean is_tos_accepted) {
	this.is_tos_accepted = is_tos_accepted;
    }

    public Set<Organization> getOrganizations() {
	return organizations;
    }

    public void setOrganizations(Set<Organization> organizations) {
	this.organizations = organizations;

    }

    public String getPostalCode() {
	return postalCode;
    }

    public String getTaxId() {
	return taxId;
    }

    public void setTaxId(String taxId) {
	this.taxId = taxId;
    }

    public String getCompanyName() {
	return companyName;
    }

    public void setCompanyName(String companyName) {
	this.companyName = companyName;
    }

    public void setPostalCode(String postalCode) {
	this.postalCode = postalCode;
    }

    public String getStreetName() {
	return streetName;
    }

    public void setStreetName(String streetName) {
	this.streetName = streetName;
    }

    public String getMunicipality() {
	return municipality;
    }

    public void setMunicipality(String municipality) {
	this.municipality = municipality;
    }

    public String getCountry() {
	return country;
    }

    public void setCountry(String country) {
	this.country = country;
    }

    public String getResetToken() {
	return resetToken;
    }

    public void setResetToken(String resetToken) {
	this.resetToken = resetToken;
    }

    public Set<Vehicle> getFavoritesVehicle() {
        return favoritesVehicle;
    }

    public void setFavoritesVehicle(Set<Vehicle> favoritesVehicle) {
        this.favoritesVehicle = favoritesVehicle;
    }

    public Set<Lote> getFavoritesLot() {
        return favoritesLot;
    }

    public void setFavoritesLot(Set<Lote> favoritesLot) {
        this.favoritesLot = favoritesLot;
    }

}
