package grupoHasten.b2bvo.application.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SecondaryTable;
import javax.persistence.SecondaryTables;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

//@EqualsAndHashCode
@Entity
@SecondaryTables({ @SecondaryTable(name = "attachments", pkJoinColumns = {
	@PrimaryKeyJoinColumn(name = "organization_id", referencedColumnName = "id") }) })
@Table(name = "organization")
public class Organization  implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "parent_id", nullable = true)
    private Long parentId;

    @Column
    private String taxId;

    @Column(name = "companyName", unique = true, nullable = false)
    private String companyName;

    @Column(name = "verified")
    private Boolean verified;

    @Column(name = "verification_cost")
    private BigDecimal verificationCost;

        
    @JsonIgnore
    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(name = "organizations_addresses", joinColumns = {
	    @JoinColumn(name = "organization_id") }, inverseJoinColumns = { @JoinColumn(name = "address_id") })
    private Set<Address> orgAddresses = new HashSet<Address>();

    public void addAddress(Address address) {
	this.orgAddresses.add(address);
	//address.getOrganizations().add(this);
    }

    public void removeAddress(Address address) {
	this.orgAddresses.remove(address);
	address.getOrganizations().remove(this);
    }

    public void removeAddresss() {
	Iterator<Address> iterator = this.orgAddresses.iterator();

	while (iterator.hasNext()) {
	    Address address = iterator.next();

	    address.getOrganizations().remove(this);
	    iterator.remove();
	}
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public Long getParentId() {
	return parentId;
    }

    public void setParentId(Long parentId) {
	this.parentId = parentId;
    }

    public String getTaxId() {
	return taxId;
    }

    public void setTaxId(String taxId) {
	this.taxId = taxId;
    }

    public String getCompanyName() {
	return companyName;
    }

    public void setCompanyName(String companyName) {
	this.companyName = companyName;
    }

    public Boolean getVerified() {
	return verified;
    }

    public void setVerified(Boolean verified) {
	this.verified = verified;
    }

    public BigDecimal getVerificationCost() {
	return verificationCost;
    }

    public void setVerificationCost(BigDecimal verificationCost) {
	this.verificationCost = verificationCost;
    }

    /**
     * @return the orgAddresses
     */
    public Set<Address> getOrgAddresses() {
	return orgAddresses;
    }

    /**
     * @param orgAddresses the orgAddresses to set
     */
    public void setOrgAddresses(Set<Address> orgAddresses) {
	this.orgAddresses = orgAddresses;
    }

}
