package grupoHasten.b2bvo.application.model;

public class ChangePasswordRequest {

    private String resetToken;
    private String email;
    private String password;

    public String getResetToken() {
	return resetToken;
    }

    public void setResetToken(String resetToken) {
	this.resetToken = resetToken;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
	return password;
    }

    public void setPassword(String password) {
	this.password = password;
    }

}
