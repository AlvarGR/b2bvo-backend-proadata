package grupoHasten.b2bvo.application.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "NegotiationMessage")
public class NegotiationMessage implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "sender_id", foreignKey = @ForeignKey(name = "fk_negotiationmessage_sender"))
    private ApplicationUser sender;

    @Column(columnDefinition = "TEXT")
    private String message;

    @Column
    private Date fechaEmision;

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public ApplicationUser getSender() {
	return sender;
    }

    public void setSender(ApplicationUser sender) {
	this.sender = sender;
    }

    public String getMessage() {
	return message;
    }

    public void setMessage(String message) {
	this.message = message;
    }

    public Date getFechaEmision() {
	return fechaEmision;
    }

    public void setFechaEmision(Date fechaEmision) {
	this.fechaEmision = fechaEmision;
    }

}
