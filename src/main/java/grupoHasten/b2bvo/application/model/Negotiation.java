package grupoHasten.b2bvo.application.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import grupoHasten.b2bvo.application.enumerates.NegotiationState;

@SuppressWarnings("serial")
@Entity
@Table(name = "negotiation", uniqueConstraints = @UniqueConstraint(name = "UK_identification", columnNames = {
	"identification" }))
public class Negotiation implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 15)
    private String identification;

    @Column
    private Date fechaInicio;

    @Column
    private Date fechaUltimoMensaje;

    @Column(name = "state")
    @Enumerated(value = EnumType.STRING)
    private NegotiationState state;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "comprador_id", foreignKey = @ForeignKey(name = "fk_negotiation_comprador"))
    private Organization comprador;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "vendedor_id", foreignKey = @ForeignKey(name = "fk_negotiation_vendedor"))
    private Organization vendedor;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "contacto_vendedor_id", foreignKey = @ForeignKey(name = "fk_negotiation_contactovendedor"))
    private ApplicationUser contactoVendedor;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "contacto_comprador_id", foreignKey = @ForeignKey(name = "fk_negotiation_contactocomprador"))
    private ApplicationUser contactoComprador;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    //@JoinColumn(name = "transportista_id")
    private Transportista transportista;
    
    @OneToMany(fetch = FetchType.LAZY)
    private List<Vehicle> lstVehicles;

    @OneToMany(fetch = FetchType.LAZY)
    private List<Lote> lstLotes;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<NegotiationMessage> messages = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<NegotiationDocument> documents = new HashSet<NegotiationDocument>();

    public void addMessage(NegotiationMessage message) {
	if (messages == null) {
	    messages = new ArrayList<>();
	}
	messages.add(message);
    }

    public void removeMessage(NegotiationMessage message) {
	messages.remove(message);
    }

    public void addDocument(NegotiationDocument document) {
	if (documents == null) {
	    documents = new HashSet<NegotiationDocument>();
	}
	documents.add(document);
    }

    public void removeDocument(NegotiationDocument document) {
	documents.remove(document);
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getIdentification() {
	return identification;
    }

    public void setIdentification(String identification) {
	this.identification = identification;
    }

    public Date getFechaInicio() {
	return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
	this.fechaInicio = fechaInicio;
    }

    public Date getFechaUltimoMensaje() {
	return fechaUltimoMensaje;
    }

    public void setFechaUltimoMensaje(Date fechaUltimoMensaje) {
	this.fechaUltimoMensaje = fechaUltimoMensaje;
    }

    public NegotiationState getState() {
	return state;
    }

    public void setState(NegotiationState state) {
	this.state = state;
    }

    public Organization getComprador() {
	return comprador;
    }

    public void setComprador(Organization comprador) {
	this.comprador = comprador;
    }

    public Transportista getTransportista() {
        return transportista;
    }

    public void setTransportista(Transportista transportista) {
        this.transportista = transportista;
    }

    public Organization getVendedor() {
	return vendedor;
    }

    public ApplicationUser getContactoVendedor() {
	return contactoVendedor;
    }

    public void setContactoVendedor(ApplicationUser contactoVendedor) {
	this.contactoVendedor = contactoVendedor;
    }

    public ApplicationUser getContactoComprador() {
	return contactoComprador;
    }

    public void setContactoComprador(ApplicationUser contactoComprador) {
	this.contactoComprador = contactoComprador;
    }

    public void setVendedor(Organization vendedor) {
        this.vendedor = vendedor;
    }

    public List<NegotiationMessage> getMessages() {
	return messages;
    }

    public void setMessages(List<NegotiationMessage> messages) {
	this.messages = messages;
    }

    public Set<NegotiationDocument> getDocuments() {
	return documents;
    }

    public void setDocuments(Set<NegotiationDocument> documents) {
	this.documents = documents;
    }

}
