
package grupoHasten.b2bvo.application.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import grupoHasten.b2bvo.application.service.impl.UserDetailsServiceImpl;

@Configuration
@EnableWebSecurity
public class WebSecurity extends WebSecurityConfigurerAdapter {

    private UserDetailsServiceImpl userDetailsService;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public WebSecurity(UserDetailsServiceImpl userDetailsService, BCryptPasswordEncoder bCryptPasswordEncoder) {
	this.userDetailsService = userDetailsService;
	this.bCryptPasswordEncoder = bCryptPasswordEncoder;
}


    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
	/*
	 * 1. Se desactiva el uso de cookies 
	 * 2. Se activa la configuración CORS con los valores por defecto 
	 * 3. Se desactiva el filtro CSRF 
	 * 4. Se indica que el login no requiere autenticación 
	 * 5. Se indica que el resto de URLs esten securizadas
	 */
	
	httpSecurity.cors().and().csrf().disable()
		.authorizeRequests()
        .antMatchers(HttpMethod.POST,  "/api/auth/signin").permitAll()
        .antMatchers(HttpMethod.POST, "/api/auth/signUp").permitAll()
        .antMatchers(HttpMethod.GET, "/api/auth/users").permitAll()
        // Rafael para la demo
        .antMatchers(HttpMethod.POST, "/api/car").permitAll()
        .antMatchers(HttpMethod.GET,  "/api/lot").permitAll()
//        .anyRequest().authenticated()
        .and();
 //       .addFilter(new JWTAuthenticationFilter(authenticationManager()))
 //       .addFilter(new JWTAuthorizationFilter(authenticationManager()))
        // this disables session creation on Spring Security
//        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
	// Se define la clase que recupera los usuarios y el algoritmo para procesar las passwords
	auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
    }

//    @Bean
//    CorsConfigurationSource corsConfigurationSource() {
//            CorsConfiguration configuration = new CorsConfiguration();
//            configuration.setAllowedOrigins(Arrays.asList("*"));
//            configuration.setAllowedMethods(Arrays.asList("GET", "POST", "OPTIONS", "DELETE", "PUT", "PATCH"));
//            configuration.setAllowedHeaders(Arrays.asList("X-Requested-With", "Origin", "Content-Type", "Accept", "Authorization"));
//            configuration.setAllowCredentials(true);
//            UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
//            source.registerCorsConfiguration("/**", configuration);
//            return source;
//        }
}
