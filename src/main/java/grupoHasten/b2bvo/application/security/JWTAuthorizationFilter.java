package grupoHasten.b2bvo.application.security;

import static grupoHasten.b2bvo.application.security.SecurityConstants.HEADER_STRING;
import static grupoHasten.b2bvo.application.security.SecurityConstants.SECRET;
import static grupoHasten.b2bvo.application.security.SecurityConstants.TOKEN_PREFIX;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

    public JWTAuthorizationFilter(AuthenticationManager authManager) {
	super(authManager);
    }

    @Override
    protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain)
	    throws IOException, ServletException {
	String header = req.getHeader(HEADER_STRING);

	if (header == null || !header.startsWith(TOKEN_PREFIX)) {
	    chain.doFilter(req, res);
	    return;
	}

	res.setHeader("Access-Control-Allow-Origin", "*");
	res.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
	res.setHeader("Access-Control-Max-Age", "3600");
	res.setHeader("Access-Control-Allow-Credentials", "true");
	res.setHeader("Access-Control-Allow-Headers","Origin,Accept,X-Requested-With,Content-Type,Access-Control-Request-Method,Access-Control-Request-Headers,Authorization");
	
	if (req.getMethod().equals("OPTIONS")) {
	    res.setStatus(HttpServletResponse.SC_ACCEPTED);
	    return;
	}
	UsernamePasswordAuthenticationToken authentication = getAuthentication(req);
	SecurityContextHolder.getContext().setAuthentication(authentication);
	chain.doFilter(req, res);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
	
	String token = request.getHeader(HEADER_STRING);
	if (token != null) {
	    // parse the token.
	   // String tokenAux = token.replace(TOKEN_PREFIX, "");
	    String user = JWT.require(Algorithm.HMAC512(SECRET.getBytes()))
                    .build()
                    .verify(token.replace(TOKEN_PREFIX, ""))
                    .getSubject();

	    if (user != null) {
		return new UsernamePasswordAuthenticationToken(user, null, new ArrayList<>());
	    }
	    return null;
	}
	return null;
    }
}