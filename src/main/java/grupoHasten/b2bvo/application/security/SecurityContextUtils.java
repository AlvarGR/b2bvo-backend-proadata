package grupoHasten.b2bvo.application.security;

import lombok.extern.slf4j.Slf4j;

import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

@Slf4j
public final class SecurityContextUtils {

  private SecurityContextUtils() {}

  /**
   * Get user id from security context
   * 
   * @return
   */
  public static String getAuthenticatedUserId() {
    log.info("Comprobar usuario logado getAuthenticatedUserId");
    final SecurityContext oAuth2AuthenticationDetails = SecurityContextHolder.getContext();
    return oAuth2AuthenticationDetails.getAuthentication().getName();
  }

}
