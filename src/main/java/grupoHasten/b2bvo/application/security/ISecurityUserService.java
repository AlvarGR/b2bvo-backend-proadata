package grupoHasten.b2bvo.application.security;

public interface ISecurityUserService {

    String validatePasswordResetToken(long id, String token);

}
