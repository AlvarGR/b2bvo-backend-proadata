package grupoHasten.b2bvo.application.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import grupoHasten.b2bvo.application.dtos.NegotiationDto;
import grupoHasten.b2bvo.application.dtos.NegotiationMessageDto;
import grupoHasten.b2bvo.application.service.NegotiationService;
import grupoHasten.b2bvo.application.service.UserService;
import grupoHasten.b2bvo.application.service.exception.VehicleNotFoundException;

@RestController
@RequestMapping(value = "api")
public class NegotiationController {

    @Autowired
    NegotiationService negotiationService;

    @Autowired
    UserService userService;

    @CrossOrigin
    @PostMapping("createNegotiationVehicle")
    ResponseEntity<NegotiationDto> createNegotiationVehicle(@Valid @RequestParam Long idComprador, @RequestParam Long idVehiculo,@RequestParam(required = false) Long idTransportista,
	    @RequestBody String mensaje) throws Exception {

	NegotiationDto newNegotiation = negotiationService.createNegotiationVehicle(idComprador,idVehiculo, idTransportista, mensaje);
	return (ResponseEntity<NegotiationDto>) ResponseEntity.ok(newNegotiation);
    }
    
    @CrossOrigin
    @PostMapping("createNegotiationLot")
    ResponseEntity<NegotiationDto> createNegotiationLote(@Valid @RequestParam Long idVendedor,
	    @RequestParam Long idComprador, @RequestParam Long idLote,
	    @RequestBody NegotiationMessageDto mensaje) {

	NegotiationDto newNegotiation = negotiationService.createNegotiationLote(idComprador, idVendedor,
		idLote);
	return (ResponseEntity<NegotiationDto>) ResponseEntity.ok(newNegotiation);
    }


    @CrossOrigin
    @PostMapping("getNegotiation")
    ResponseEntity<NegotiationDto> getNegotiation(@RequestParam Long id) {

	return ResponseEntity.ok(negotiationService.getNegotiation(id));

    }

    @CrossOrigin
    @PostMapping("getMessageNegotiation")
    ResponseEntity<NegotiationDto> getMessageNegotiation(@Valid @RequestBody NegotiationDto negotiationDto) {

//	NegotiationDto newNegotiation = negotiationService.createNegotiation(negotiationDto);

	return null;

    }

    @CrossOrigin
    @PostMapping("saveMessageNegotiation")
    ResponseEntity<NegotiationDto> saveMessageNegotiation(@Valid @RequestBody NegotiationMessageDto negotiationMessage) {

	


	return null;

    }

    @CrossOrigin
    @PostMapping("getDocumentationNegotiation")
    ResponseEntity<NegotiationDto> getDocumentationNegotiation(@Valid @RequestBody NegotiationDto negotiationDto) {

//	NegotiationDto newNegotiation = negotiationService.createNegotiation(negotiationDto);

	return null;

    }

    @CrossOrigin
    @PostMapping("getTransportistaNegotiation")
    ResponseEntity<NegotiationDto> getTransportistaNegotiation(@Valid @RequestBody NegotiationDto negotiationDto) {

//	NegotiationDto newNegotiation = negotiationService.createNegotiation(negotiationDto);

	return null;

    }

    @CrossOrigin
    @PostMapping("getCompradorNegotiation")
    ResponseEntity<NegotiationDto> getCompradorNegotiation(@Valid @RequestBody NegotiationDto negotiationDto) {

//	NegotiationDto newNegotiation = negotiationService.createNegotiation(negotiationDto);

	return null;

    }

    @CrossOrigin
    @PostMapping("getVendedorNegotiation")
    ResponseEntity<NegotiationDto> getVendedorNegotiation(@Valid @RequestBody NegotiationDto negotiationDto) {

//	NegotiationDto newNegotiation = negotiationService.createNegotiation(negotiationDto);

	return null;

    }

    @CrossOrigin
    @PostMapping("getOrganizationCompradorNegotiation")
    ResponseEntity<NegotiationDto> getOrganizationCompradorNegotiation(
	    @Valid @RequestBody NegotiationDto negotiationDto) {

//	NegotiationDto newNegotiation = negotiationService.createNegotiation(negotiationDto);

	return null;

    }

    @CrossOrigin
    @PostMapping("getOrganizationVendedorNegotiation")
    ResponseEntity<NegotiationDto> getOrganizationVendedorNegotiation(
	    @Valid @RequestBody NegotiationDto negotiationDto) {

//	NegotiationDto newNegotiation = negotiationService.createNegotiation(negotiationDto);

	return null;

    }

}
