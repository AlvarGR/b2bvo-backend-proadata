package grupoHasten.b2bvo.application.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import grupoHasten.b2bvo.application.facade.model.JatoFilter;
import grupoHasten.b2bvo.application.facade.model.JatoMake;
import grupoHasten.b2bvo.application.facade.model.JatoMakeModel;
import grupoHasten.b2bvo.application.facade.model.JatoModel;
import grupoHasten.b2bvo.application.facade.model.JatoVersion;
import grupoHasten.b2bvo.application.service.JatoService;

@RestController
@RequestMapping(value="api")
public class JatoController {
	
	@Autowired
	JatoService jatoService;
	
	@CrossOrigin
	@GetMapping("makes")
	ResponseEntity<List<JatoMake>> obtenerJatoMakes() {
		List<JatoMake> makes = jatoService.obtenerMakes();
		return (ResponseEntity<List<JatoMake>>) ResponseEntity.ok(makes);
	}
	
	@CrossOrigin
	@GetMapping("models")
	ResponseEntity<List<JatoModel>> obtenerJatoModel(@RequestParam String make) {
		List<JatoModel> models = jatoService.obtenerModel(make);
		return (ResponseEntity<List<JatoModel>>) ResponseEntity.ok(models);
	}
	
	@CrossOrigin
	@GetMapping("filters")
	ResponseEntity<JatoFilter> obtenerJatoFilter(
			@RequestParam String make,
			@RequestParam String model) {
		JatoFilter filter = jatoService.obtenerFilter(make, model);
		return (ResponseEntity<JatoFilter>) ResponseEntity.ok(filter);
	}

	@CrossOrigin
	@GetMapping("versions")
	ResponseEntity<List<JatoVersion>> obtenerJatoVersion(
			@RequestParam String make,
			@RequestParam String model) {
		List<JatoVersion> version = jatoService.obtenerVersion(make, model);
		return (ResponseEntity<List<JatoVersion>>) ResponseEntity.ok(version);
	}

	@CrossOrigin
	@GetMapping("makemodel")
	ResponseEntity<List<JatoMakeModel>> obtenerJatoMakeModel() {
		List<JatoMakeModel> makeModel = jatoService.obtenerJatoMakeModel();
		return (ResponseEntity<List<JatoMakeModel>>) ResponseEntity.ok(makeModel);
	}
}
