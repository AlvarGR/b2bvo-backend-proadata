package grupoHasten.b2bvo.application.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import grupoHasten.b2bvo.application.dtos.MisBusquedasDto;
import grupoHasten.b2bvo.application.service.MisBusquedasService;

@RestController
@RequestMapping(value="api")
public class MisBusquedasController {

	@Autowired
	MisBusquedasService misBusquedasService;
	
	@CrossOrigin
	@PostMapping("misbusquedas")
	ResponseEntity<MisBusquedasDto> createMisBusquedas(@Valid @RequestBody  MisBusquedasDto misBusquedasDto) {
		MisBusquedasDto misBusquedasDtoNew = misBusquedasService.saveMisBusquedas(misBusquedasDto);
		return (ResponseEntity<MisBusquedasDto >) ResponseEntity.ok(misBusquedasDtoNew);
	}

	@CrossOrigin
	@GetMapping("misbusquedas")
	ResponseEntity<List<MisBusquedasDto>> getMisBusquedasAll(
			@RequestParam(value = "user") String user) {
		List<MisBusquedasDto> misBusquedasListDto = misBusquedasService.getMisBusquedasAll(user);
		return (ResponseEntity<List<MisBusquedasDto> >) ResponseEntity.ok(misBusquedasListDto);
	}

	@CrossOrigin
	@GetMapping("misbusquedas/{id}")
	ResponseEntity<MisBusquedasDto> getMisBusquedas(@PathVariable("id") Long id) {
		MisBusquedasDto misBusquedasListDto = misBusquedasService.getMisBusquedas(id);
		return (ResponseEntity<MisBusquedasDto>) ResponseEntity.ok(misBusquedasListDto);
	}

	@CrossOrigin
	@DeleteMapping("misbusquedas/{id}")
	ResponseEntity<?> deleteMisBusquedas(@PathVariable("id") Long id) {
		misBusquedasService.removeMisBusquedas(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

}
