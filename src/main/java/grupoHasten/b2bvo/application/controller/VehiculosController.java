package grupoHasten.b2bvo.application.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import grupoHasten.b2bvo.application.dtos.SearchCriteriaDto;
import grupoHasten.b2bvo.application.dtos.VehicleDto;
import grupoHasten.b2bvo.application.service.VehiclePaginationService;
import grupoHasten.b2bvo.application.service.exception.VehicleNotFoundException;
import grupoHasten.b2bvo.application.service.impl.VehicleServiceImpl;

@RestController
@RequestMapping(value="api")
public class VehiculosController {

	@Autowired
	VehicleServiceImpl vehicleServiceImpl;

	@Autowired
	VehiclePaginationService vehiclePaginationService;
	
	@CrossOrigin
	@PostMapping("car/search")
	ResponseEntity<Page<VehicleDto>> getVehicles(
			@RequestParam(value = "page") Long page,
			@RequestBody(required = false)  SearchCriteriaDto filters) {
		Page<VehicleDto> pageVehiculosDto = vehiclePaginationService.getVehiclesFilter(page, filters);
		return (ResponseEntity<Page<VehicleDto> >) ResponseEntity.ok(pageVehiculosDto);
	}

	@CrossOrigin
	@PostMapping("car")
	ResponseEntity<VehicleDto> saveVehicle(@Valid @RequestBody VehicleDto vehicleDto) {
		
		VehicleDto vehiculoDtoNew = null;
		vehiculoDtoNew = vehicleServiceImpl.saveVehicle(vehicleDto);
		return (ResponseEntity<VehicleDto>) ResponseEntity.ok(vehiculoDtoNew);
	}

	@CrossOrigin
	@GetMapping("car/{id}")
	ResponseEntity<VehicleDto> getVehicle(@PathVariable("id") Long id)  {

		VehicleDto vehiculo = null;
		try {
			vehiculo = vehicleServiceImpl.getVehicle(id);
		} catch (VehicleNotFoundException ex) {
			throw new ResponseStatusException(
			           HttpStatus.NOT_FOUND, ex.getMessage(), ex);
		}
		return (ResponseEntity<VehicleDto>) ResponseEntity.ok(vehiculo);
	}

}
