package grupoHasten.b2bvo.application.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import grupoHasten.b2bvo.application.dtos.LoteDto;
import grupoHasten.b2bvo.application.dtos.VehicleDto;
import grupoHasten.b2bvo.application.model.ApplicationUser;
import grupoHasten.b2bvo.application.service.FavoritesService;

@RestController
@RequestMapping(value="/api/favorites")
public class FavoritesController {

    
    @Autowired
    FavoritesService favoritesService;
    
//listar Vehicles favorites
    @GetMapping(value = "/vehicle", produces = "application/json")
    @CrossOrigin
    @ResponseBody
    ResponseEntity<List<VehicleDto>>favoritesVehicle(@RequestParam("idUser") String idUser) {	
	return ResponseEntity.ok(favoritesService.lstFavoritesVehicle(Long.parseLong(idUser)));
    }
    
//añadir vehiculo a favoritos
    @PostMapping(value = "/addVehicle", produces = "application/json")
    @CrossOrigin
    @ResponseBody
    ResponseEntity<ApplicationUser>AddFavoritesVehicle(@RequestParam("idUser") String idUser, @RequestParam("idVehicle") String idVehicle) {
	return ResponseEntity.ok(favoritesService.addFavoriteVehicle(Long.parseLong(idUser), Long.parseLong(idVehicle)));
	
    }
    
//Eliminar vehiculo de favoritos
    @DeleteMapping(value = "/removeVehicle", produces = "application/json")
    @CrossOrigin
    @ResponseBody
    ResponseEntity<List<VehicleDto>>RemoveVehicle(@RequestParam("idUser") String idUser, @RequestParam("idVehicle") String idVehicle) {
	return ResponseEntity.ok(favoritesService.removeFavoriteVehicle(Long.parseLong(idUser), Long.parseLong(idVehicle)));
    }
    
 //listar Lotes favorites
    @GetMapping(value = "/lots", produces = "application/json")
    @CrossOrigin
    @ResponseBody
    ResponseEntity<List<LoteDto>>favoritesLotes(@RequestParam("idUser") String idUser) {	
	return ResponseEntity.ok(favoritesService.lstFavoritesLotes(Long.parseLong(idUser)));
    }
    
//añadir Lote a favoritos
    @PostMapping(value = "/addLot", produces = "application/json")
    @CrossOrigin
    @ResponseBody
    ResponseEntity<ApplicationUser>AddFavoritesLot(@RequestParam("idUser") String idUser, @RequestParam("idLote") String idLote) {
	return ResponseEntity.ok(favoritesService.addFavoriteLot(Long.parseLong(idUser), Long.parseLong(idLote)));
    }
    
//Eliminar Lote de favoritos
    @DeleteMapping(value = "/removeLot", produces = "application/json")
    @CrossOrigin
    @ResponseBody
    ResponseEntity<List<LoteDto>>RemoveLot(@RequestParam("idUser") String idUser, @RequestParam("idLote") String idLote) {
	return ResponseEntity.ok(favoritesService.removeFavoriteLote(Long.parseLong(idUser), Long.parseLong(idLote)));
    }
    
//Consultar si el coche es favorito.
    @GetMapping(value = "/askFavoriteCar", produces = "application/json")
    @CrossOrigin
    @ResponseBody
    ResponseEntity<Boolean> QueryFavoriteCar(@RequestParam("idUser") String idUser, @RequestParam("idVehicle") String idLote) {
	return ResponseEntity.ok(favoritesService.consultaVehiculoFavorito(Long.parseLong(idUser), Long.parseLong(idLote)));
    }
    
//Consultar si el lote es favorito
    @GetMapping(value = "/askFavoriteLot", produces = "application/json")
    @CrossOrigin
    @ResponseBody
    ResponseEntity<Boolean> QueryFavoriteLot(@RequestParam("idUser") String idUser, @RequestParam("idLote") String idLote) {
	return ResponseEntity.ok(favoritesService.consultaLoteFavorito(Long.parseLong(idUser), Long.parseLong(idLote)));
    }
    
}