package grupoHasten.b2bvo.application.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import grupoHasten.b2bvo.application.dtos.VehicleDto;
import grupoHasten.b2bvo.application.service.VehicleService;

@CrossOrigin
@Api(tags = "Logged user api")
@RequestMapping(value = "/api/user")
@RestController
public class UserController {

  @Autowired
  VehicleService vehicleService;


  @ApiOperation(value = "Retrieves paginated and filtered vehicles by logged user")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "OK"),
      @ApiResponse(code = 400, message = "Bad Request", response = ResponseEntity.class),
      @ApiResponse(code = 503, message = "Service unavailable", response = ResponseEntity.class)})
  @GetMapping("/vehicle")
  ResponseEntity<Page<VehicleDto>> getAllVehicles(
      @ApiParam(name = "userId", value = "Id user", required = true) @RequestParam(value = "userId",
          required = true) Long userId,
      @ApiParam(name = "pageIndex", value = "Number of page", required = false) @RequestParam(
          value = "pageIndex", required = false, defaultValue = "0") int pageIndex) {
    Page<VehicleDto> vehicles = vehicleService.findVehicleByUserId(pageIndex, userId);
    return ResponseEntity.ok(vehicles);
  }

}
