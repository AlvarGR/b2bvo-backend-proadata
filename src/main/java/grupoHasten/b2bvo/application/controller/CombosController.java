package grupoHasten.b2bvo.application.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import grupoHasten.b2bvo.application.enumerates.VehicleState;

@RestController
@RequestMapping("api")
public class CombosController {
	
	@SuppressWarnings("serial")
	class ComboDto implements Serializable {
		private String key;
		private String value;
		
		ComboDto(String key, String value) {
			this.key = key;
			this.value = value;
		}

		public String getKey() {
			return key;
		}

		public void setKey(String key) {
			this.key = key;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}
		
		
	}
	
	@CrossOrigin
    @GetMapping("/vehiclestate")
    public ResponseEntity<List<ComboDto>> getVehicleStateValues() {
		List<VehicleState> values = new ArrayList<>(Arrays.asList(VehicleState.values()));
		List<ComboDto> statesDto = new ArrayList<>();
		for (VehicleState item: values) {
			statesDto.add(new ComboDto(item.getKey(), item.getState()));
		}
		return ResponseEntity.ok(statesDto);
    }


}
