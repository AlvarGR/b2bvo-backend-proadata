package grupoHasten.b2bvo.application.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.services.s3.model.S3ObjectSummary;

import grupoHasten.b2bvo.application.dtos.ResponseBuckerDto;
import grupoHasten.b2bvo.application.service.AmazonClient;

@RestController
@RequestMapping("/api/storage")
public class BucketController {

	@Autowired
	private AmazonClient amazonClient;
	
	@CrossOrigin
    @PostMapping("/uploadFile")
    public ResponseEntity<ResponseBuckerDto> uploadFile(@RequestParam(value = "file") MultipartFile file) {
		ResponseBuckerDto result = null;
        try {
        	result = amazonClient.uploadFile(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ResponseEntity.ok(result);
    }

	@CrossOrigin
    @GetMapping(value = "/downloadFile", produces = MediaType.IMAGE_JPEG_VALUE)
    public @ResponseBody byte[] downloadFile(@RequestParam(value = "key") String key) {
		
		byte[] bytes = null;
		
		try {
			bytes = amazonClient.downloadFile(key);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        return bytes;
    }

	@CrossOrigin
    @GetMapping("/list")
    public ResponseEntity<List<S3ObjectSummary>> list() {
		
		List<S3ObjectSummary> fotosAlmacenadas = amazonClient.list();
		return ResponseEntity.ok(fotosAlmacenadas);
    }

	@CrossOrigin
    @DeleteMapping("/deleteFile")
    public ResponseEntity<ResponseBuckerDto> deleteFile(@RequestParam(value = "key") String key) {
        return ResponseEntity.ok(amazonClient.deleteFileFromS3Bucket(key)); 
    }	
}
