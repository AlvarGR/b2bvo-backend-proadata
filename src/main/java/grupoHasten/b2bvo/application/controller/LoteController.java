package grupoHasten.b2bvo.application.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import grupoHasten.b2bvo.application.dtos.LoteDto;
import grupoHasten.b2bvo.application.service.LoteService;
import grupoHasten.b2bvo.application.service.exception.LoteNotFoundException;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

@CrossOrigin
@RestController
@Api(tags = "Lote api")
@RequestMapping(value = "api")
public class LoteController {

  @Autowired
  LoteService loteService;


  @PostMapping("lot")
  ResponseEntity<LoteDto> saveLote(@Valid @RequestBody LoteDto loteDto) {
    LoteDto newLoteDto = loteService.saveLote(loteDto);
    return ResponseEntity.ok(newLoteDto);
  }

  @GetMapping("lot")
  ResponseEntity<List<LoteDto>> getAllLotes() {
    List<LoteDto> lotesDto = loteService.obtenerLotes();
    return ResponseEntity.ok(lotesDto);
  }

  @ApiOperation(value = "Retrieves paginated and filtered Lotes ")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "OK"),
      @ApiResponse(code = 400, message = "Bad Request", response = ResponseEntity.class),
      @ApiResponse(code = 503, message = "Service unavailable", response = ResponseEntity.class)})
  @GetMapping("lot/search")
  ResponseEntity<Page<LoteDto>> searchLotes(
      @RequestParam(value = "make", required = false) String make,
      @RequestParam(value = "model", required = false) String model,
      @RequestParam(value = "version", required = false) String version,
      @RequestParam(value = "registrationDateFrom", required = false) Date registrationDateFrom,
      @RequestParam(value = "registrationDateTo", required = false) Date registrationDateTo,
      @RequestParam(value = "basePriceFrom", required = false) Double basePriceFrom,
      @RequestParam(value = "basePriceTo", required = false) Double basePriceTo,
      @RequestParam(value = "fuel", required = false) String fuel,
      @RequestParam(value = "kmFrom", required = false) Long kmFrom,
      @RequestParam(value = "kmTo", required = false) Long kmTo,
      @RequestParam(value = "numberOfDoorsFrom", required = false) String numberOfDoorsFrom,
      @RequestParam(value = "numberOfDoorsTo", required = false) String numberOfDoorsTo,
      @RequestParam(value = "seatingCapacityFrom", required = false) Long seatingCapacityFrom,
      @RequestParam(value = "seatingCapacityTo", required = false) Long seatingCapacityTo,
      @ApiParam(name = "pageIndex", value = "Number of page", required = false) @RequestParam(
          value = "pageIndex", required = false, defaultValue = "0") int pageIndex) {

    Page<LoteDto> lotes = loteService.searchLote(make, model, version, registrationDateFrom,
        registrationDateTo, basePriceFrom, basePriceTo, fuel, kmFrom, kmTo, numberOfDoorsFrom,
        numberOfDoorsTo, seatingCapacityFrom, seatingCapacityTo, pageIndex);
    return ResponseEntity.ok(lotes);
  }

  @GetMapping("lot/{id}")
  ResponseEntity<LoteDto> getLote(@PathVariable Long id) {
    LoteDto loteDto;
    try {
      loteDto = loteService.obtenerLote(id);
    } catch (LoteNotFoundException ex) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage(), ex);
    }
    return ResponseEntity.ok(loteDto);
  }


  @PutMapping("lot/{id}")
  ResponseEntity<LoteDto> getUpdateLote(@PathVariable Long id, @RequestBody LoteDto loteDto) {
    LoteDto newLoteDto;
    try {
      newLoteDto = loteService.actualizarLote(id, loteDto);
    } catch (LoteNotFoundException ex) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage(), ex);
    }

    return ResponseEntity.ok(newLoteDto);
  }

  @DeleteMapping("lot/{id}")
  ResponseEntity<LoteDto> getDeleteLote(@PathVariable Long id) {

    return ResponseEntity.ok(null);
  }

}
