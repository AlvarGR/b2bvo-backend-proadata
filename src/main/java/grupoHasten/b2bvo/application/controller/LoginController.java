package grupoHasten.b2bvo.application.controller;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import grupoHasten.b2bvo.application.model.Address;
import grupoHasten.b2bvo.application.model.ApplicationUser;
import grupoHasten.b2bvo.application.model.ChangePasswordRequest;
import grupoHasten.b2bvo.application.model.GenericResponse;
import grupoHasten.b2bvo.application.model.LoginRequest;
import grupoHasten.b2bvo.application.model.Organization;
import grupoHasten.b2bvo.application.model.PasswordResetToken;
import grupoHasten.b2bvo.application.model.TokenResponse;
import grupoHasten.b2bvo.application.repository.PasswordResetTokenRepository;
import grupoHasten.b2bvo.application.service.UserService;
import grupoHasten.b2bvo.application.service.impl.EmailSenderServiceImpl;
import grupoHasten.b2bvo.application.service.impl.UserServiceImpl;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.TextCodec;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.POST, RequestMethod.GET })
@RequestMapping("/api/auth")
public class LoginController {

    @Value("${jwt.secret}")
    private String secret;

//    @Autowired
//    private MessageSource messages;

//    @Autowired
//    private Environment env;

    @Autowired
    private PasswordResetTokenRepository passwordTokenRepository;

    // private JavaMailSender mailSender;

    @Autowired
    UserService userService;

    @Autowired
    EmailSenderServiceImpl emailService;

//    @Autowired
//    UserSecurityService securityService;

//    @Autowired
//    BCryptPasswordEncoder bCryptPasswordEncoder;

    public LoginController(UserServiceImpl userService) { // , BCryptPasswordEncoder bCryptPasswordEncoder
	this.userService = userService;
//	this.bCryptPasswordEncoder = bCryptPasswordEncoder;		
    }

    @CrossOrigin(origins = "*", methods = { RequestMethod.POST })
    @PostMapping(value = "/signin", consumes = "application/json", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<TokenResponse> authenticateUser(@RequestBody LoginRequest loginRequest)
	    throws ServletException {

	Organization organizacion = new Organization();
	TokenResponse response = new TokenResponse();
	ApplicationUser existUSer = null;
	try {
	    existUSer = userService.existUser(loginRequest.getUsernameOrEmail(), loginRequest.getPassword());
	} catch (Exception e) {
	    e.printStackTrace();// TODO: handle exception
	}
	if (existUSer == null) {
	    return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
	}

	Iterator<Organization> empresa = existUSer.getOrganizations().iterator();
	
	 organizacion = empresa.next();
//	if (empresa.hasNext()) {
	   
//	} else {
//	    organizacion = null;
//	}
	final Instant now = Instant.now();
	final String jwt = Jwts.builder().setSubject(existUSer.getName()).setIssuedAt(Date.from(now))
		.setSubject(existUSer.getName()).claim("CompanyName", existUSer.getCompanyName())
		.claim("OrganizationName", organizacion.getCompanyName()).claim("OrganizationId", organizacion.getId())
		.claim("Country", existUSer.getCountry()).claim("Email", existUSer.getEmail())
		.claim("IdUser", existUSer.getId()).claim("UserName", existUSer.getName())
		.claim("LastName", existUSer.getLastName()).claim("Municipality", existUSer.getMunicipality())
		.claim("National_id", existUSer.getNational_id()).claim("Position", existUSer.getPosition())
		.setExpiration(Date.from(now.plus(1, ChronoUnit.DAYS)))
		.signWith(SignatureAlgorithm.HS256, TextCodec.BASE64.encode(secret)).compact() + "\"";
	response.setToken(jwt);
	return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<List<ApplicationUser>> listarUsuarios() {

	List<ApplicationUser> lstUsuarios = userService.listarUsuarios();
	return new ResponseEntity<List<ApplicationUser>>(lstUsuarios, HttpStatus.OK);
	// new ResponseEntity<JSONObject>(lstUsuarios, HttpStatus.OK);;

    }

    @RequestMapping(value = "/user/resetPassword", method = RequestMethod.GET)
    @CrossOrigin
    @ResponseBody
    public GenericResponse password_reset(HttpServletRequest request, @RequestParam("email") String userEmail) {
	final ApplicationUser user = userService.findUserByEmail(userEmail);
	if (user != null) {
	    final String token = UUID.randomUUID().toString();
	    userService.createPasswordResetTokenForUser(user, token);
	    emailService.sendEmail(userEmail, constructResetTokenEmail(getAppUrl(request), token, user));
	}
	return new GenericResponse("Te enviaremos un correo electrónico para restablecer su contraseña");
    }

    @RequestMapping(value = "/user/changeResetPassword", method = RequestMethod.POST)
    @CrossOrigin
    @ResponseBody
    public ResponseEntity<ApplicationUser> password_reset_change(@RequestBody ChangePasswordRequest request) {

	try {
	    ApplicationUser usuarioCambio = userService.findUserByResetToken("a09d1eaf-9a85-45ff-b0a0-35582dee28e4");
	    if (usuarioCambio != null && usuarioCambio.getEmail().equalsIgnoreCase(request.getEmail())) {
		usuarioCambio.setPassword(request.getPassword());
		usuarioCambio.setResetToken("");
		return new ResponseEntity<>(userService.saveUser(usuarioCambio), HttpStatus.OK);
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    private String getAppUrl(HttpServletRequest request) {
	return "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
    }

    public void createPasswordResetTokenForUser(ApplicationUser user, String token) {
	PasswordResetToken myToken = new PasswordResetToken(token, user);
	passwordTokenRepository.save(myToken);
    }

    private String constructResetTokenEmail(String contextPath, String token, ApplicationUser user) {
	String url = contextPath + "/auth/reset/" + token;
	String message = "Restablecer contraseña";
	return message + " \r\n" + url;
    }

    @ResponseBody
    @CrossOrigin(origins = "*", methods = { RequestMethod.POST })
    @PostMapping(value = "/signup", consumes = "application/json", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<ApplicationUser> saveUser(@RequestBody ApplicationUser applicationUser) {

	ApplicationUser usuarioSave = new ApplicationUser();

	Organization empresa = new Organization();

	Address address = new Address();

	Date date = new Date();

	usuarioSave.setName(applicationUser.getName());
	usuarioSave.setPassword(applicationUser.getPassword());
	usuarioSave.setCreated_at(date);
	usuarioSave.setUpdated_at(date);
	usuarioSave.setEmail(applicationUser.getEmail());
	usuarioSave.setLastName(applicationUser.getLastName());
	usuarioSave.setActive(true);
	usuarioSave.setPosition(applicationUser.getPosition());
	usuarioSave.setNational_id(applicationUser.getNational_id());
	usuarioSave.setCreated_at(date);
	usuarioSave.setUpdated_at(date);
	usuarioSave.setActive(true);

	empresa.setTaxId(applicationUser.getTaxId());
	empresa.setCompanyName(applicationUser.getCompanyName());

	address.setPostalCode(applicationUser.getPostalCode());
	address.setStreet(applicationUser.getStreetName());
	address.setMunicipality(applicationUser.getMunicipality());
	address.setCountry(applicationUser.getCountry());

	empresa.addAddress(address);

	usuarioSave.addOrganization(empresa);

	// userService.saveUser(usuarioSave);

	ApplicationUser auxUser = new ApplicationUser();
	try {
	    auxUser = userService.saveUser(usuarioSave);
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return new ResponseEntity<>(auxUser, HttpStatus.OK);
    }

}
