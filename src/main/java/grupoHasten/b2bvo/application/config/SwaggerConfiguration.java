package grupoHasten.b2bvo.application.config;

import com.google.common.base.Predicate;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@ComponentScan(basePackages = {"grupoHasten.b2bvo.application.controller"})
public class SwaggerConfiguration {

  private static final String API_TITLE = "B2bvo API";
  private static final String API_VERSION = "1.0";
  /* Do not publish swagger for token APIs */
  private static final String API_PATHS_REGEX = "/api.*";

  @Bean
  public Docket getApiDefinition() {
    return new Docket(DocumentationType.SWAGGER_2).select().paths(getApiPaths()).build();
  }

  /**
   * This is necessary in order to filter the documented paths. If not spring-boot-actuator endpoints (and others)
   * would be shown, when what we really want to document is the functional API
   */
  private static Predicate<String> getApiPaths() {
      return PathSelectors.regex(API_PATHS_REGEX);
  }
}