package grupoHasten.b2bvo.application.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import grupoHasten.b2bvo.application.model.ApplicationUser;
import grupoHasten.b2bvo.application.model.MisBusquedas;

public interface MisBusquedasRepository extends JpaRepository<MisBusquedas, Long> {
	List<MisBusquedas> findByUser(ApplicationUser user);
	MisBusquedas findByName(String name);
}
