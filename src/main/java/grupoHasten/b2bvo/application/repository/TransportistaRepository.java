package grupoHasten.b2bvo.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import grupoHasten.b2bvo.application.model.Transportista;

public interface TransportistaRepository extends JpaRepository<Transportista, Long> {

}
