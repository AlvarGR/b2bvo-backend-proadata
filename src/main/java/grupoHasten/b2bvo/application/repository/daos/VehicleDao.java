package grupoHasten.b2bvo.application.repository.daos;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.dsl.BooleanExpression;

import grupoHasten.b2bvo.application.dtos.SearchCriteriaDto;
import grupoHasten.b2bvo.application.model.Vehicle;
import grupoHasten.b2bvo.application.repository.VehicleSearchRepository;
import grupoHasten.b2bvo.application.repository.querydsl.QVehicle;

@Component
public class VehicleDao {

	@Autowired
	VehicleSearchRepository vehicleSearchRepository;
	
	public Page<Vehicle> getAllVehicles(Pageable pageable, SearchCriteriaDto criteria) {
		
		
		BooleanBuilder predicate = new BooleanBuilder();
		
		if (criteria != null) {

			if (criteria.getPrecioDesde() != null && criteria.getPrecioHasta() != null) {
				predicate.and(isPrizeBetween(criteria.getPrecioDesde(), criteria.getPrecioHasta()));
			}
			
			if (criteria.getKilometrajeDesde() != null && criteria.getKilometrajeHasta() != null) {
				predicate.and(isMilleageBetween(criteria.getKilometrajeDesde(), criteria.getKilometrajeHasta()));
			}
			
			if (criteria.getNumberOfDoorsDesde() != null && criteria.getNumberOfDoorsHasta() != null) {
				predicate.and(isNumberOfDoorsBetween(criteria.getNumberOfDoorsDesde(), criteria.getNumberOfDoorsHasta()));
			}
			
			if (criteria.getSeatingCapacityDesde() != null && criteria.getSeatingCapacityHasta() != null) {
				predicate.and(isSeatingCapacityBetween(criteria.getSeatingCapacityDesde(), criteria.getSeatingCapacityHasta()));
			}
	
			if (criteria.getSeatingCapacityDesde() != null && criteria.getSeatingCapacityHasta() != null) {
				predicate.and(isSeatingCapacityBetween(criteria.getSeatingCapacityDesde(), criteria.getSeatingCapacityHasta()));
			}
	
			if (criteria.getFechaMatriculacionDesde() != null && criteria.getFechaMatriculacionHasta() != null) {
				predicate.and(isFechaMatriculacionBetween(criteria.getFechaMatriculacionDesde(), criteria.getFechaMatriculacionHasta()));
			}
	
			if (criteria.getMake() != null ) {
				predicate.and(isMakeEquals(criteria.getMake()));
			}
	
			if (criteria.getModel() != null ) {
				predicate.and(isModelEquals(criteria.getModel()));
			}
	
			if (criteria.getVersion() != null ) {
				predicate.and(isVersionEquals(criteria.getVersion()));
			}
		}
//		OrderSpecifier<Long> orderSpecifier = QVehicle.vehicle.id.asc();
		Page<Vehicle> Vehicles = vehicleSearchRepository.findAll(predicate, pageable);
	    return Vehicles;
	}
	
	private BooleanExpression isPrizeBetween(Double prizeFrom, Double prizeTo) {
		return QVehicle.vehicle.basePrice.between(prizeFrom, prizeTo);
	}
	
	private BooleanExpression isMilleageBetween(Long milleageFrom, Long milleageTo) {
		return QVehicle.vehicle.milleage.between(milleageFrom, milleageTo);
	}
	
	private BooleanExpression isNumberOfDoorsBetween(String numberOfDoorsFrom, String numberOfDoorsTo) {
		return QVehicle.vehicle.engine.numberOfDoors.between(numberOfDoorsFrom, numberOfDoorsTo);
	}
	
	private BooleanExpression isSeatingCapacityBetween(Long seatingCapacityFrom, Long seatingCapacityTo) {
		return QVehicle.vehicle.engine.seatingCapacity.between(seatingCapacityFrom, seatingCapacityTo);
	}
	
	private BooleanExpression isFechaMatriculacionBetween(Date fechaMatriculacionFrom, Date fechaMatriculacionTo) {
		return QVehicle.vehicle.registrationDate.between(fechaMatriculacionFrom, fechaMatriculacionTo);
	}
	
	private BooleanExpression isMakeEquals(String make) {
		return QVehicle.vehicle.make.eq(make);
	}
	
	private BooleanExpression isModelEquals(String model) {
		return QVehicle.vehicle.models.eq(model);
	}
	
	private BooleanExpression isVersionEquals(String version) {
		return QVehicle.vehicle.version.eq(version);
	}
	
}
