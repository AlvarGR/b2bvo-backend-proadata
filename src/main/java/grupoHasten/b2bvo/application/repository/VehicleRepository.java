package grupoHasten.b2bvo.application.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import grupoHasten.b2bvo.application.model.Vehicle;

public interface VehicleRepository extends JpaRepository<Vehicle, Long>{
  
  Page<Vehicle> findByUploader_Id(Long id, Pageable pageable);
  
}
