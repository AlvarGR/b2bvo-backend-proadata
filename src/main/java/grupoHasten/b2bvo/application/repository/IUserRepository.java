package grupoHasten.b2bvo.application.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import grupoHasten.b2bvo.application.model.ApplicationUser;


public interface IUserRepository extends JpaRepository<ApplicationUser, Long> {

    ApplicationUser findByName(String name);

    Optional<ApplicationUser> findById(Long id);

    ApplicationUser findByEmail(String email);
    
    ApplicationUser findByResetToken(String resetToken);   
}
