package grupoHasten.b2bvo.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import grupoHasten.b2bvo.application.model.Attachment;

public interface AttachmentRepository extends JpaRepository<Attachment, Long> {

}
