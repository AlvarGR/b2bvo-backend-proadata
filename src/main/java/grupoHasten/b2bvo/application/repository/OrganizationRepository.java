package grupoHasten.b2bvo.application.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import grupoHasten.b2bvo.application.model.Organization;

public interface OrganizationRepository extends JpaRepository<Organization, Long> {
	List<Organization> findByCompanyName(String companyName);
}
