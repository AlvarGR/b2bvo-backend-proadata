package grupoHasten.b2bvo.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import grupoHasten.b2bvo.application.model.NegotiationMessage;

public interface NegotiationMessageRepository extends JpaRepository<NegotiationMessage, Long>{

}
