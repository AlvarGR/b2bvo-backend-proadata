package grupoHasten.b2bvo.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import grupoHasten.b2bvo.application.model.VehicleType;

public interface VehicleTypeRepository extends JpaRepository<VehicleType, Long> {
	VehicleType findByType(String type);
}
