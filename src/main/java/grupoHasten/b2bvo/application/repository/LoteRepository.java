package grupoHasten.b2bvo.application.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import grupoHasten.b2bvo.application.model.Lote;

import java.util.Date;

@Repository
public interface LoteRepository extends JpaRepository<Lote, Long> {
  
  @Query("SELECT l FROM Lote l WHERE l.id in (Select v.lote from Vehicle v "
      + "where (v.make=:make OR :make is null) "
      + "AND (v.models=:model OR :model is null)"
      + "AND (v.version=:version OR :version is null)"
      + "AND (v.engine.fuelType=:fuel OR :fuel is null)"
      + "AND (v.engine.numberOfDoors>:numberOfDoorsFrom OR :numberOfDoorsFrom is null) "
      + "AND (v.engine.numberOfDoors<:numberOfDoorsTo OR :numberOfDoorsTo is null)"
      + "AND (v.engine.seatingCapacity>:seatingCapacityFrom OR :seatingCapacityFrom is null) "
      + "AND (v.engine.seatingCapacity<:seatingCapacityTo OR :seatingCapacityTo is null)"
      + "AND (v.milleage>:kmFrom OR :kmFrom is null)"
      + "AND (v.milleage<:kmTo OR :kmTo is null)"
      + "AND (v.registrationDate>:registrationDateFrom OR :registrationDateFrom is null)"
      + "AND (v.registrationDate<:registrationDateTo OR :registrationDateTo is null)"
      + "AND (v.basePrice>:basePriceFrom OR :basePriceFrom is null)"
      + "AND (v.basePrice<:basePriceTo OR :basePriceTo is null)"
      + ")")
  Page<Lote> searchLote( 
      @Param(value = "make") String make,
      @Param(value = "model") String model,
      @Param(value = "version") String version,
      @Param(value = "registrationDateFrom") Date registrationDateFrom,
      @Param(value = "registrationDateTo") Date registrationDateTo,
      @Param(value = "basePriceFrom") Double basePriceFrom,
      @Param(value = "basePriceTo") Double basePriceTo,
      @Param(value = "fuel") String fuel,
      @Param(value = "kmFrom") Long kmFrom,
      @Param(value = "kmTo") Long kmTo,
      @Param(value = "numberOfDoorsFrom") String numberOfDoorsFrom,
      @Param(value = "numberOfDoorsTo") String numberOfDoorsTo,
      @Param(value = "seatingCapacityFrom") Long seatingCapacityFrom,
      @Param(value = "seatingCapacityTo") Long seatingCapacityTo, 
      Pageable pageable);
  
}
