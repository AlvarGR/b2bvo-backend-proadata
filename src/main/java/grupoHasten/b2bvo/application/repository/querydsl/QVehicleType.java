package grupoHasten.b2bvo.application.repository.querydsl;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;

import javax.annotation.Generated;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.ListPath;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.PathInits;
import com.querydsl.core.types.dsl.StringPath;

import grupoHasten.b2bvo.application.model.Vehicle;
import grupoHasten.b2bvo.application.model.VehicleType;


/**
 * QVehicleType is a Querydsl query type for VehicleType 
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QVehicleType extends EntityPathBase<VehicleType> {

    private static final long serialVersionUID = -1413906946L;

    public static final QVehicleType vehicleType = new QVehicleType("vehicleType");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath type = createString("type");

    public final ListPath<Vehicle, QVehicle> vehicles = this.<Vehicle, QVehicle>createList("vehicles", Vehicle.class, QVehicle.class, PathInits.DIRECT2);

    public QVehicleType(String variable) {
        super(VehicleType.class, forVariable(variable));
    }

    public QVehicleType(Path<? extends VehicleType> path) {
        super(path.getType(), path.getMetadata());
    }

    public QVehicleType(PathMetadata metadata) {
        super(VehicleType.class, metadata);
    }

}

