package grupoHasten.b2bvo.application.repository.querydsl;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;

import javax.annotation.Generated;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.ListPath;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.PathInits;
import com.querydsl.core.types.dsl.StringPath;

import grupoHasten.b2bvo.application.model.Attachment;
import grupoHasten.b2bvo.application.model.AttachmentType;


/**
 * QAttachmentType is a Querydsl query type for AttachmentType 
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAttachmentType extends EntityPathBase<AttachmentType> {

    private static final long serialVersionUID = 1296408197L;

    public static final QAttachmentType attachmentType = new QAttachmentType("attachmentType");

    public final ListPath<Attachment, QAttachment> attachments = this.<Attachment, QAttachment>createList("attachments", Attachment.class, QAttachment.class, PathInits.DIRECT2);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath type = createString("type");

    public QAttachmentType(String variable) {
        super(AttachmentType.class, forVariable(variable));
    }

    public QAttachmentType(Path<? extends AttachmentType> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAttachmentType(PathMetadata metadata) {
        super(AttachmentType.class, metadata);
    }

}

