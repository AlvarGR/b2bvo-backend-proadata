package grupoHasten.b2bvo.application.repository.querydsl;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;

import javax.annotation.Generated;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;

import grupoHasten.b2bvo.application.model.Engine;


/**
 * QEngine is a Querydsl query type for Engine 
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QEngine extends EntityPathBase<Engine> {

    private static final long serialVersionUID = 353209802L;

    public static final QEngine engine = new QEngine("engine");

    public final NumberPath<Long> co2Emission = createNumber("co2Emission", Long.class);

    public final StringPath compressor = createString("compressor");

    public final NumberPath<Long> engineCC = createNumber("engineCC", Long.class);

    public final StringPath fuelType = createString("fuelType");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath litres = createString("litres");

    public final NumberPath<Long> maximumPowerCv = createNumber("maximumPowerCv", Long.class);

    public final NumberPath<Long> maximumPowerKw = createNumber("maximumPowerKw", Long.class);

    public final NumberPath<Long> numberOfCylinders = createNumber("numberOfCylinders", Long.class);

    public final StringPath numberOfDoors = createString("numberOfDoors");

    public final NumberPath<Long> seatingCapacity = createNumber("seatingCapacity", Long.class);

    public final StringPath transmissionType = createString("transmissionType");

    public QEngine(String variable) {
        super(Engine.class, forVariable(variable));
    }

    public QEngine(Path<? extends Engine> path) {
        super(path.getType(), path.getMetadata());
    }

    public QEngine(PathMetadata metadata) {
        super(Engine.class, metadata);
    }

}

