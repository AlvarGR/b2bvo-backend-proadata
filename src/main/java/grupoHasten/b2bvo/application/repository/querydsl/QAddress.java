package grupoHasten.b2bvo.application.repository.querydsl;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;

import javax.annotation.Generated;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.PathInits;
import com.querydsl.core.types.dsl.SetPath;
import com.querydsl.core.types.dsl.StringPath;

import grupoHasten.b2bvo.application.model.Address;
import grupoHasten.b2bvo.application.model.Organization;


/**
 * QAddress is a Querydsl query type for Address
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAddress extends EntityPathBase<Address> {

    private static final long serialVersionUID = -1479247508L;

    public static final QAddress address = new QAddress("address");

    public final StringPath country = createString("country");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath municipality = createString("municipality");

    public final NumberPath<Integer> numero = createNumber("numero", Integer.class);

    public final SetPath<Organization, QOrganization> organizations = this.<Organization, QOrganization>createSet("organizations", Organization.class, QOrganization.class, PathInits.DIRECT2);

    public final StringPath postalCode = createString("postalCode");

    public final NumberPath<Integer> provincia = createNumber("provincia", Integer.class);

    public final StringPath street = createString("street");

    public final StringPath town = createString("town");

    public QAddress(String variable) {
        super(Address.class, forVariable(variable));
    }

    public QAddress(Path<? extends Address> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAddress(PathMetadata metadata) {
        super(Address.class, metadata);
    }

}

