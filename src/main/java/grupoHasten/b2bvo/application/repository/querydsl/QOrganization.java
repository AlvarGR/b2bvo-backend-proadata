package grupoHasten.b2bvo.application.repository.querydsl;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;

import javax.annotation.Generated;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.BooleanPath;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.PathInits;
import com.querydsl.core.types.dsl.SetPath;
import com.querydsl.core.types.dsl.StringPath;

import grupoHasten.b2bvo.application.model.Address;
import grupoHasten.b2bvo.application.model.Organization;


/**
 * QOrganization is a Querydsl query type for Organization 
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QOrganization extends EntityPathBase<Organization> {

    private static final long serialVersionUID = -1957709125L;

    public static final QOrganization organization = new QOrganization("organization");

    public final StringPath companyName = createString("companyName");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final SetPath<Address, QAddress> orgAddresses = this.<Address, QAddress>createSet("orgAddresses", Address.class, QAddress.class, PathInits.DIRECT2);

    public final NumberPath<Long> parentId = createNumber("parentId", Long.class);

    public final StringPath taxId = createString("taxId");

    public final NumberPath<java.math.BigDecimal> verificationCost = createNumber("verificationCost", java.math.BigDecimal.class);

    public final BooleanPath verified = createBoolean("verified");

    public QOrganization(String variable) {
        super(Organization.class, forVariable(variable));
    }

    public QOrganization(Path<? extends Organization> path) {
        super(path.getType(), path.getMetadata());
    }

    public QOrganization(PathMetadata metadata) {
        super(Organization.class, metadata);
    }

}

