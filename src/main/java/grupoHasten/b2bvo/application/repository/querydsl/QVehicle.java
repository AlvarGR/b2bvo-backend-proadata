package grupoHasten.b2bvo.application.repository.querydsl;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;

import javax.annotation.Generated;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.BooleanPath;
import com.querydsl.core.types.dsl.DateTimePath;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.EnumPath;
import com.querydsl.core.types.dsl.ListPath;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.PathInits;
import com.querydsl.core.types.dsl.StringPath;

import grupoHasten.b2bvo.application.model.Attachment;
import grupoHasten.b2bvo.application.model.Vehicle;


/**
 * QVehicle is a Querydsl query type for Vehicle
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QVehicle extends EntityPathBase<Vehicle> {

    private static final long serialVersionUID = 10513572L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QVehicle vehicle = new QVehicle("vehicle");

    public final ListPath<Attachment, QAttachment> attachments = this.<Attachment, QAttachment>createList("attachments", Attachment.class, QAttachment.class, PathInits.DIRECT2);

    public final DateTimePath<java.util.Date> availabilityDate = createDateTime("availabilityDate", java.util.Date.class);

    public final NumberPath<Double> basePrice = createNumber("basePrice", Double.class);

    public final StringPath comments = createString("comments");

    public final NumberPath<Double> discountAmount = createNumber("discountAmount", Double.class);

    public final NumberPath<Double> discountPercentage = createNumber("discountPercentage", Double.class);

    public final QEngine engine;

    public final NumberPath<Double> finalPrice = createNumber("finalPrice", Double.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final QLote lote;

    public final StringPath make = createString("make");

    public final NumberPath<Long> milleage = createNumber("milleage", Long.class);

    public final StringPath models = createString("models");

    public final StringPath plateNumber = createString("plateNumber");

    public final DateTimePath<java.util.Date> registrationDate = createDateTime("registrationDate", java.util.Date.class);

    public final QOrganization seller;

    public final EnumPath<grupoHasten.b2bvo.application.enumerates.VehicleState> vehicleState = createEnum("vehicleState", grupoHasten.b2bvo.application.enumerates.VehicleState.class);

    public final QVehicleType vehicleType;

    public final StringPath version = createString("version");

    public final BooleanPath visible = createBoolean("visible");

    public QVehicle(String variable) {
        this(Vehicle.class, forVariable(variable), INITS);
    }

    public QVehicle(Path<? extends Vehicle> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QVehicle(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QVehicle(PathMetadata metadata, PathInits inits) {
        this(Vehicle.class, metadata, inits);
    }

    public QVehicle(Class<? extends Vehicle> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.engine = inits.isInitialized("engine") ? new QEngine(forProperty("engine")) : null;
        this.lote = inits.isInitialized("lote") ? new QLote(forProperty("lote"), inits.get("lote")) : null;
        this.seller = inits.isInitialized("seller") ? new QOrganization(forProperty("seller")) : null;
        this.vehicleType = inits.isInitialized("vehicleType") ? new QVehicleType(forProperty("vehicleType")) : null;
    }

}

