package grupoHasten.b2bvo.application.repository.querydsl;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;

import javax.annotation.Generated;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.BooleanPath;
import com.querydsl.core.types.dsl.DateTimePath;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.PathInits;
import com.querydsl.core.types.dsl.SetPath;
import com.querydsl.core.types.dsl.StringPath;

import grupoHasten.b2bvo.application.model.ApplicationUser;
import grupoHasten.b2bvo.application.model.Organization;


/**
 * QApplicationUser is a Querydsl query type for ApplicationUser
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QApplicationUser extends EntityPathBase<ApplicationUser> {

    private static final long serialVersionUID = 969745075L;

    public static final QApplicationUser applicationUser = new QApplicationUser("applicationUser");

    public final BooleanPath active = createBoolean("active");

    public final DateTimePath<java.util.Date> created_at = createDateTime("created_at", java.util.Date.class);

    public final StringPath email = createString("email");

    public final BooleanPath emailVerified = createBoolean("emailVerified");

    public final BooleanPath enabled = createBoolean("enabled");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final BooleanPath is_tos_accepted = createBoolean("is_tos_accepted");

    public final StringPath lastName = createString("lastName");

    public final StringPath name = createString("name");

    public final StringPath national_id = createString("national_id");

    public final SetPath<Organization, QOrganization> organizations = this.<Organization, QOrganization>createSet("organizations", Organization.class, QOrganization.class, PathInits.DIRECT2);

    public final StringPath password = createString("password");

    public final StringPath position = createString("position");

    public final StringPath resetToken = createString("resetToken");

    public final DateTimePath<java.util.Date> updated_at = createDateTime("updated_at", java.util.Date.class);

    public QApplicationUser(String variable) {
        super(ApplicationUser.class, forVariable(variable));
    }

    public QApplicationUser(Path<? extends ApplicationUser> path) {
        super(path.getType(), path.getMetadata());
    }

    public QApplicationUser(PathMetadata metadata) {
        super(ApplicationUser.class, metadata);
    }

}

