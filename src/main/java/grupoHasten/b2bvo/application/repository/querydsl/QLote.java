package grupoHasten.b2bvo.application.repository.querydsl;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;

import javax.annotation.Generated;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.DateTimePath;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.ListPath;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.PathInits;
import com.querydsl.core.types.dsl.StringPath;

import grupoHasten.b2bvo.application.model.Lote;
import grupoHasten.b2bvo.application.model.Vehicle;


/**
 * QLote is a Querydsl query type for Lote
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QLote extends EntityPathBase<Lote> {

    private static final long serialVersionUID = 612867260L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QLote lote = new QLote("lote");

    public final DateTimePath<java.util.Date> availabilityDate = createDateTime("availabilityDate", java.util.Date.class);

    public final NumberPath<Double> basePrice = createNumber("basePrice", Double.class);

    public final QApplicationUser contactPerson;

    public final StringPath deliveryPlace = createString("deliveryPlace");

    public final NumberPath<Double> discountAmount = createNumber("discountAmount", Double.class);

    public final NumberPath<Double> discountPercentage = createNumber("discountPercentage", Double.class);

    public final NumberPath<Double> finalPrice = createNumber("finalPrice", Double.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath name = createString("name");

    public final DateTimePath<java.util.Date> publicationDate = createDateTime("publicationDate", java.util.Date.class);

    public final ListPath<Vehicle, QVehicle> vehicles = this.<Vehicle, QVehicle>createList("vehicles", Vehicle.class, QVehicle.class, PathInits.DIRECT2);

    public QLote(String variable) {
        this(Lote.class, forVariable(variable), INITS);
    }

    public QLote(Path<? extends Lote> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QLote(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QLote(PathMetadata metadata, PathInits inits) {
        this(Lote.class, metadata, inits);
    }

    public QLote(Class<? extends Lote> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.contactPerson = inits.isInitialized("contactPerson") ? new QApplicationUser(forProperty("contactPerson")) : null;
    }

}

