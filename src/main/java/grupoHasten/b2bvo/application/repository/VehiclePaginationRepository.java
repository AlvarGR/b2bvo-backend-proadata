package grupoHasten.b2bvo.application.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import grupoHasten.b2bvo.application.model.Vehicle;

public interface VehiclePaginationRepository extends PagingAndSortingRepository<Vehicle, Long> {
	
}
