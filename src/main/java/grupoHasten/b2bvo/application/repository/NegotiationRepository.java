package grupoHasten.b2bvo.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import grupoHasten.b2bvo.application.model.Negotiation;

public interface NegotiationRepository extends JpaRepository<Negotiation, Long>{
   

}
