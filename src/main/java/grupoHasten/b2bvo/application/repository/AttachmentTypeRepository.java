package grupoHasten.b2bvo.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import grupoHasten.b2bvo.application.model.AttachmentType;

public interface AttachmentTypeRepository extends JpaRepository<AttachmentType, Long> {
	AttachmentType findByType(String type);
}
