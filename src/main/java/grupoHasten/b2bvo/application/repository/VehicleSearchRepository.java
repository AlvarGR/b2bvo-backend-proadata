package grupoHasten.b2bvo.application.repository;

import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import grupoHasten.b2bvo.application.model.Vehicle;

public interface VehicleSearchRepository 
		extends PagingAndSortingRepository<Vehicle, Long>, QuerydslPredicateExecutor<Vehicle> {

}
