package grupoHasten.b2bvo.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import grupoHasten.b2bvo.application.model.Engine;

public interface EngineRepository extends JpaRepository<Engine, Long> {

}
