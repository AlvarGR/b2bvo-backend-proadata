package grupoHasten.b2bvo.application.dtos;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

@SuppressWarnings("serial")
public class MisBusquedasDto implements Serializable {
	private Long id;
	
	@NotNull
	private String user;
	
	@NotNull
	private String name;
	
	@NotNull
	private SearchCriteriaDto search;
	
	public SearchCriteriaDto getSearch() {
		return search;
	}
	public void setSearch(SearchCriteriaDto search) {
		this.search = search;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	
}
