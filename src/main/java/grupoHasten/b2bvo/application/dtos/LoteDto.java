package grupoHasten.b2bvo.application.dtos;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import grupoHasten.b2bvo.application.model.Organization;

@SuppressWarnings("serial")
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoteDto implements Serializable {

  private Long id;

  @NotNull
  private String name;

  @NotNull
  private ImporteDto price;

  @NotNull
  private Long uploader;

  @NotNull
  private Date publicationDate;

  @NotNull
  private Date availabilityDate;

  @NotNull
  private String contactPerson;

  private String nameContactPerson;

  @NotNull
  private String deliveryPlace;

  private List<VehicleDto> vehiclesDto;

  private Organization organizacionDto;

}
