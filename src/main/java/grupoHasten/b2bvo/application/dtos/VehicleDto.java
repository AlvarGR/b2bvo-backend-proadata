package grupoHasten.b2bvo.application.dtos;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

@SuppressWarnings("serial")
public class VehicleDto implements Serializable {
	private Long id;
	
	@NotNull
	private String seller;
	
	@NotNull
	private String make;

	@NotNull
	private Long uploader;
	
	@NotNull
	private String model;
	
	@NotNull
	private String version;

	private String versionLabel;
	
	@NotNull
	private Date registrationDate;
	
	@NotNull
	private Date availabilityDate;
	
	@NotNull
	private String plateNumber;

	@NotNull
	private Long milleage;

	@NotNull
	private ImporteDto price;

	@NotNull
	private String typeVehicle;
	
	private EngineDto engine;
	
	private String comments;
    
	@NotNull
    private String state;

	@NotNull
	private List<AttachmentDto> attachments;
	
	public String getSeller() {
		return seller;
	}
	public void setSeller(String seller) {
		this.seller = seller;
	}
	public String getMake() {
		return make;
	}
	public void setMake(String make) {
		this.make = make;
	}
	public Long getUploader() {
	    return uploader;
	}
	public void setUploader(Long uploader) {
	    this.uploader = uploader;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public Date getRegistrationDate() {
		return registrationDate;
	}
	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}
	public String getPlateNumber() {
		return plateNumber;
	}
	public void setPlateNumber(String plateNumber) {
		this.plateNumber = plateNumber;
	}
	public Long getMilleage() {
		return milleage;
	}
	public void setMilleage(Long milleage) {
		this.milleage = milleage;
	}
	public String getTypeVehicle() {
		return typeVehicle;
	}
	public void setTypeVehicle(String typeVehicle) {
		this.typeVehicle = typeVehicle;
	}
	public List<AttachmentDto> getAttachments() {
		return attachments;
	}
	public void setAttachments(List<AttachmentDto> attachments) {
		this.attachments = attachments;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public EngineDto getEngine() {
		return engine;
	}
	public void setEngine(EngineDto engine) {
		this.engine = engine;
	}
	public Date getAvailabilityDate() {
		return availabilityDate;
	}
	public void setAvailabilityDate(Date availabilityDate) {
		this.availabilityDate = availabilityDate;
	}
	public String getVersionLabel() {
		return versionLabel;
	}
	public void setVersionLabel(String versionLabel) {
		this.versionLabel = versionLabel;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public ImporteDto getPrice() {
		return price;
	}
	public void setPrice(ImporteDto price) {
		this.price = price;
	}	
}
