package grupoHasten.b2bvo.application.dtos;

import java.util.Date;

import javax.validation.constraints.NotNull;

import grupoHasten.b2bvo.application.model.ApplicationUser;

public class NegotiationMessageDto {

    @NotNull
    private Long idNegotiation;
    
    private Long id;

    @NotNull
    private ApplicationUserDto sender; 
    
    @NotNull
    private String message;
    
    @NotNull
    private Date fechaEmision;

    public Long getIdNegotiation() {
        return idNegotiation;
    }

    public void setIdNegotiation(Long idNegotiation) {
        this.idNegotiation = idNegotiation;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ApplicationUserDto getSender() {
        return sender;
    }

    public void setSender(ApplicationUserDto sender) {
        this.sender = sender;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(Date fechaEmision) {
        this.fechaEmision = fechaEmision;
    }
    
    
}
