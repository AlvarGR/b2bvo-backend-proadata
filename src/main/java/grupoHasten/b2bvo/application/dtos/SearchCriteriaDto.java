package grupoHasten.b2bvo.application.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class SearchCriteriaDto implements Serializable {
	private String make;
	private String model;
	private String version;
	private Date fechaMatriculacionDesde;
	private Date fechaMatriculacionHasta;
	private Double precioDesde;
	private Double precioHasta;
	private String combustible;
	private Long kilometrajeDesde;
	private Long kilometrajeHasta;
	private String numberOfDoorsDesde;
	private String numberOfDoorsHasta;
	private Long seatingCapacityDesde;
	private Long seatingCapacityHasta;
}
