package grupoHasten.b2bvo.application.dtos.convert;

import org.springframework.data.domain.Page;

import grupoHasten.b2bvo.application.dtos.LoteDto;
import grupoHasten.b2bvo.application.model.Lote;

public interface LoteConvert {
	Lote transformDtoToEntity(LoteDto loteDto);
	LoteDto transformEntityToDto(Lote lote);
	Page<LoteDto> transformEntityToDtoLst(Page<Lote> lote);
	public Lote actualizarEntity(Lote lote, LoteDto loteDto);
}
