package grupoHasten.b2bvo.application.dtos.convert.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import grupoHasten.b2bvo.application.dtos.AttachmentDto;
import grupoHasten.b2bvo.application.dtos.EngineDto;
import grupoHasten.b2bvo.application.dtos.ImporteDto;
import grupoHasten.b2bvo.application.dtos.VehicleDto;
import grupoHasten.b2bvo.application.dtos.convert.VehicleConvert;
import grupoHasten.b2bvo.application.enumerates.VehicleState;
import grupoHasten.b2bvo.application.facade.JatoFacade;
import grupoHasten.b2bvo.application.facade.model.JatoVehicleEquipmentCustomInfo;
import grupoHasten.b2bvo.application.facade.model.JatoVersion;
import grupoHasten.b2bvo.application.model.Attachment;
import grupoHasten.b2bvo.application.model.Engine;
import grupoHasten.b2bvo.application.model.Organization;
import grupoHasten.b2bvo.application.model.Vehicle;
import grupoHasten.b2bvo.application.model.VehicleType;
import grupoHasten.b2bvo.application.repository.AttachmentTypeRepository;
import grupoHasten.b2bvo.application.repository.OrganizationRepository;
import grupoHasten.b2bvo.application.repository.VehicleTypeRepository;
import grupoHasten.b2bvo.application.service.ImporteService;

@Component
public class VehicleConvertImpl implements VehicleConvert {

  @Autowired
  JatoFacade jatoFacade;

  @Autowired
  AttachmentTypeRepository attachmentTypeRepository;

  @Autowired
  OrganizationRepository organizationRepository;

  @Autowired
  VehicleTypeRepository vehicleTypeRepository;

  @Autowired
  ImporteService importeService;

  @Override
  public Vehicle transformDtoToEntity(VehicleDto vehicleDto) {
    Vehicle vehicle = new Vehicle();

    vehicleDto.setPrice(importeService.calcularImporte(vehicleDto.getPrice()));

    vehicle.setId(vehicleDto.getId());
    vehicle.setSeller(obtenerSeller(vehicleDto.getSeller()));
    vehicle.setMake(vehicleDto.getMake());
    vehicle.setModels(vehicleDto.getModel());
    vehicle.setVersion(vehicleDto.getVersion());
    vehicle.setRegistrationDate(new Date());
    vehicle.setAvailabilityDate(vehicleDto.getAvailabilityDate());
    vehicle.setPlateNumber(vehicleDto.getPlateNumber());
    vehicle.setMilleage(vehicleDto.getMilleage());
    rellenaImporte(vehicle, vehicleDto);
    vehicle.setVehicleType(getVehicleType(vehicleDto.getTypeVehicle()));
    vehicle.setVisible(Boolean.TRUE);
    vehicle.setAttachments(getAttachments(vehicleDto.getAttachments()));
    vehicle.setComments(vehicleDto.getComments());
    vehicle.setEngine(getEngine(vehicleDto.getVersion()));
    vehicle.setVehicleState(VehicleState.obtenerState(vehicleDto.getState()));
    return vehicle;
  }

  @Override
  public VehicleDto transformEntityToDto(Vehicle vehicle) {
    VehicleDto vehicleDto = new VehicleDto();
    vehicleDto.setId(vehicle.getId());
    vehicleDto.setSeller(vehicle.getSeller().getCompanyName());
    vehicleDto.setMake(vehicle.getMake());
    vehicleDto.setModel(vehicle.getModels());
    vehicleDto.setRegistrationDate(new Date());
    vehicleDto.setAvailabilityDate(vehicle.getAvailabilityDate());
    vehicleDto.setPlateNumber(vehicle.getPlateNumber());
    vehicleDto.setMilleage(vehicle.getMilleage());
    vehicleDto.setTypeVehicle(vehicle.getVehicleType().getType());
    vehicleDto.setState(vehicle.getVehicleState().getKey());
    vehicleDto.setComments(vehicle.getComments());
    vehicleDto.setVersion(vehicle.getVersion());

    vehicleDto.setEngine(getEngineDto(vehicle.getEngine()));
    vehicleDto.setPrice(rellenaImporteDto(vehicle));
    vehicleDto.setAttachments(getAttachmentsDto(vehicle.getAttachments()));
    vehicleDto.setVersionLabel(
        obtenerVersionLabel(vehicle.getMake(), vehicle.getModels(), vehicle.getVersion()));

    return vehicleDto;
  }

  @Override
  public List<VehicleDto> transformListEntitiesToListDto(List<Vehicle> vehicles) {
    List<VehicleDto> vehiclesDto = new ArrayList<>();
    for (Vehicle item : vehicles) {
      vehiclesDto.add(transformEntityToDto(item));
    }

    return vehiclesDto;
  }
  
  @Override
  public Page<VehicleDto> pageEntitiesToPageDto(Page<Vehicle> vehicles) {
    return vehicles.map(this::transformEntityToDto);

  }

  private void rellenaImporte(Vehicle vehicle, VehicleDto vehicleDto) {
    vehicle.setBasePrice(vehicleDto.getPrice().getPrecioBase());
    vehicle.setFinalPrice(vehicleDto.getPrice().getPrecioFinal());
    vehicle.setDiscountAmount(vehicleDto.getPrice().getDescuentoCantidad());
    vehicle.setDiscountPercentage(vehicleDto.getPrice().getDescuentoPorcentaje());

  }

  private ImporteDto rellenaImporteDto(Vehicle vehicle) {
    ImporteDto importeDto = new ImporteDto();

    importeDto.setPrecioBase(vehicle.getBasePrice());
    importeDto.setPrecioFinal(vehicle.getFinalPrice());
    importeDto.setDescuentoCantidad(vehicle.getDiscountAmount());
    importeDto.setDescuentoPorcentaje(vehicle.getDiscountPercentage());

    return importeDto;

  }

  private String obtenerVersionLabel(String make, String model, String version) {
    List<JatoVersion> jatoVersion = jatoFacade.obtenerVersion(make, model);
    Long vehicleId = Long.parseLong(version);
    Iterator<JatoVersion> iterator = jatoVersion.iterator();
    while (iterator.hasNext()) {
      JatoVersion item = iterator.next();
      if (item.getVehicleId().equals(vehicleId)) {
        return item.getLocalVersionName();
      }
    }
    return null;
  }

  private Engine getEngine(String version) {

    JatoVehicleEquipmentCustomInfo customInfo =
        jatoFacade.obtenerJatoVehicleEquipmentCustomInfo(version);
    Engine engine = new Engine();

    if (customInfo.getCo2Emission() != null) {
      String co2EmissionStr = String.valueOf(customInfo.getCo2Emission());
      engine.setCo2Emission(Long.parseLong(co2EmissionStr));
    }

    if (customInfo.getCompressor() != null) {
      String compresorStr = String.valueOf(customInfo.getCompressor());
      engine.setCompressor(compresorStr);
    }

    if (customInfo.getEngineCC() != null) {
      String engineCCStr = String.valueOf(customInfo.getEngineCC());
      engine.setEngineCC(Long.parseLong(engineCCStr));
    }

    if (customInfo.getFuelType() != null) {
      engine.setFuelType(customInfo.getFuelType().getId());
    }

    if (customInfo.getLitres() != null) {
      engine.setLitres(String.valueOf(customInfo.getLitres().toString()));
    }

    if (customInfo.getMaximumPowerCv() != null) {
      String maximumPowerCvStr = String.valueOf(customInfo.getMaximumPowerCv());
      engine.setMaximumPowerCv(Long.parseLong(maximumPowerCvStr));
    }

    if (customInfo.getMaximumPowerKw() != null) {
      String maximumPowerKwStr = String.valueOf(customInfo.getMaximumPowerKw());
      engine.setMaximumPowerKw(Long.parseLong(maximumPowerKwStr));
    }

    if (customInfo.getNumberOfCylinders() != null) {
      String numberOfCylindersStr = String.valueOf(customInfo.getNumberOfCylinders());
      engine.setNumberOfCylinders(Long.parseLong(numberOfCylindersStr));
    }

    if (customInfo.getTransmissionType() != null) {
      engine.setTransmissionType(customInfo.getTransmissionType().getId());
    }

    if (customInfo.getNumberOfDoors() != null) {
      engine.setNumberOfDoors(customInfo.getNumberOfDoors());
    }

    if (customInfo.getSeatingCapacity() != null) {
      engine.setSeatingCapacity(customInfo.getSeatingCapacity());
    }

    return engine;
  }

  private EngineDto getEngineDto(Engine engine) {
    EngineDto engineDto = new EngineDto();
    engineDto.setCo2Emission(engine.getCo2Emission());
    engineDto.setCompressor(engine.getCompressor());
    engineDto.setEngineCC(engine.getEngineCC());
    engineDto.setFuelType(engine.getFuelType());
    engineDto.setLitres(engine.getLitres());
    engineDto.setMaximumPowerCv(engine.getMaximumPowerCv());
    engineDto.setMaximumPowerKw(engine.getMaximumPowerKw());
    engineDto.setNumberOfCylinders(engine.getNumberOfCylinders());
    engineDto.setTransmissionType(engine.getTransmissionType());
    engineDto.setNumberOfDoors(engine.getNumberOfDoors());
    engineDto.setSeatingCapacity(engine.getSeatingCapacity());
    return engineDto;
  }

  private List<Attachment> getAttachments(List<AttachmentDto> attachmentsDto) {
    List<Attachment> attachements = new ArrayList<>();
    for (AttachmentDto attachmentDto : attachmentsDto) {
      Attachment attachment = new Attachment();
      attachment.setAttachmentType(attachmentTypeRepository.findByType(attachmentDto.getType()));
      attachment.setUrl(attachmentDto.getUrl());
      attachements.add(attachment);
    }
    return attachements;
  }

  private List<AttachmentDto> getAttachmentsDto(List<Attachment> attachments) {
    return attachments.stream().map(
        a -> AttachmentDto.builder().type(a.getAttachmentType().getType()).url(a.getUrl()).build())
        .collect(Collectors.toList());

  }

  private VehicleType getVehicleType(String typeVehicle) {
    return vehicleTypeRepository.findByType(typeVehicle);
  }

  private Organization obtenerSeller(String organizacion) {
    List<Organization> organizations = organizationRepository.findByCompanyName(organizacion);
    return organizations.get(0);
  }

  


}
