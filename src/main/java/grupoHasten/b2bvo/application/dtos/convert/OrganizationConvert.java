package grupoHasten.b2bvo.application.dtos.convert;

import grupoHasten.b2bvo.application.dtos.OrganizationDto;
import grupoHasten.b2bvo.application.model.Organization;

public interface OrganizationConvert {

    public OrganizationDto transformEntityToDto(Organization organization);

    public Organization transformDtoToEntity(OrganizationDto organizationDto);

}
