package grupoHasten.b2bvo.application.dtos.convert.impl;

import org.springframework.stereotype.Component;

import grupoHasten.b2bvo.application.dtos.OrganizationDto;
import grupoHasten.b2bvo.application.dtos.convert.OrganizationConvert;
import grupoHasten.b2bvo.application.model.Organization;

@Component
public class OrganizationConvertImpl implements OrganizationConvert {
    
    
    public OrganizationDto transformEntityToDto(Organization organization) {
	
	OrganizationDto organizationDto = new OrganizationDto();
	
	organizationDto.setId(organization.getId());
	organizationDto.setCompanyName(organization.getCompanyName());
	organizationDto.setParentId(organization.getParentId());
	organizationDto.setTaxId(organization.getTaxId());
	organizationDto.setVerificationCost(organization.getVerificationCost());
	organizationDto.setVerified(organization.getVerified());
	organizationDto.setOrgAddresses(organization.getOrgAddresses());
	
	return organizationDto;
    }
    
    public Organization transformDtoToEntity(OrganizationDto organizationDto) {
	
   	Organization organization = new Organization();
   	
   	organization.setId(organizationDto.getId());
   	organization.setCompanyName(organizationDto.getCompanyName());
   	organization.setParentId(organizationDto.getParentId());
   	organization.setTaxId(organizationDto.getTaxId());
   	organization.setVerificationCost(organizationDto.getVerificationCost());
   	organization.setVerified(organizationDto.getVerified());
   	organization.setOrgAddresses(organizationDto.getOrgAddresses());
   	
   	return organization;
       }

    

}
