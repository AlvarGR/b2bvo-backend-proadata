package grupoHasten.b2bvo.application.dtos.convert;

import java.util.List;

import grupoHasten.b2bvo.application.dtos.MisBusquedasDto;
import grupoHasten.b2bvo.application.model.ApplicationUser;
import grupoHasten.b2bvo.application.model.MisBusquedas;

public interface MisBusquedasConvert {
	MisBusquedas transformDtoToEntity(MisBusquedasDto misBusquedasDto);
	MisBusquedasDto transformEntityToDto(MisBusquedas misBusquedas);
	List<MisBusquedasDto> transformListEntitiesToListDto(List<MisBusquedas> misBusquedas);
	ApplicationUser obtenerUser(String userName);
}
