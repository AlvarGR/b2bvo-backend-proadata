package grupoHasten.b2bvo.application.dtos.convert.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import grupoHasten.b2bvo.application.dtos.NegotiationDto;
import grupoHasten.b2bvo.application.dtos.NegotiationMessageDto;
import grupoHasten.b2bvo.application.dtos.convert.NegotiationConvert;
import grupoHasten.b2bvo.application.dtos.convert.NegotiationMessageConvert;
import grupoHasten.b2bvo.application.dtos.convert.OrganizationConvert;
import grupoHasten.b2bvo.application.dtos.convert.TransportistaConvert;
import grupoHasten.b2bvo.application.model.Negotiation;
import grupoHasten.b2bvo.application.model.NegotiationMessage;
import grupoHasten.b2bvo.application.service.OrganizationService;
import grupoHasten.b2bvo.application.service.TransportistaService;
import grupoHasten.b2bvo.application.service.UserService;

@Component
class NegotiationConvertImpl implements NegotiationConvert {

    @Autowired
    UserService userService;

    @Autowired
    TransportistaConvert transportistaConvert;

    @Autowired
    NegotiationMessageConvert messageConverter;
    
    @Autowired
    OrganizationConvert organizationConverter;
    
    @Autowired
    OrganizationService organizationService;
    
    @Autowired
    TransportistaService transportistaService;
   
    
    @Override
    public Negotiation transformDtoToEntity(NegotiationDto negotiationDto) {

	Negotiation negotiation = new Negotiation();

	negotiation.setIdentification(negotiationDto.getIdentification());
	negotiation.setFechaInicio(negotiationDto.getFechaInicio());
	negotiation.setFechaUltimoMensaje(negotiationDto.getFechaUltimoMensaje());
//	negotiation.setComprador(organizationService.obtenerOrganizacion(negotiationDto.getComprador()));
//	negotiation.setVendedor(organizationService.obtenerOrganizacion(negotiationDto.getVendedor()));
//	negotiation.setContactoComprador(userService.findUserById(negotiationDto.getContactoComprador()).get());
//	negotiation.setContactoVendedor(userService.findUserById(negotiationDto.getContactoVendedor()).get());
//	negotiation.setTransportista(transportistaService.getTransportista(negotiationDto.getTransportista()));
	negotiation.setMessages(convertListMessagesDtoToListMessage(negotiationDto.getMessages()));
	negotiation.setDocuments(negotiationDto.getDocuments());
	
	
	return negotiation;
    }

    @Override
    public NegotiationDto transformEntityToDto(Negotiation negotiation) {

	NegotiationDto negotiationDto = new NegotiationDto();

	negotiationDto.setIdentification(negotiation.getIdentification());
	negotiationDto.setFechaInicio(negotiation.getFechaInicio());
	negotiationDto.setFechaUltimoMensaje(negotiation.getFechaUltimoMensaje());
//	negotiationDto.setState(negotiation.getState());
//	negotiationDto.setComprador(organizationConverter.transformEntityToDto(negotiation.getComprador()));
//	negotiationDto.setVendedor(organizationConverter.transformEntityToDto(negotiation.getVendedor()));
//	negotiationDto.setContactoComprador(negotiation.getContactoComprador());
//	negotiationDto.setContactoVendedor(negotiation.getContactoVendedor());
//	negotiationDto.setTransportista(transportistaConvert.convertEntityToDto(negotiation.getTransportista()));

	negotiationDto.setMessages(convertListMessagesToListMessagesDto(negotiation.getMessages()));

	negotiationDto.setDocuments(negotiation.getDocuments());

	return negotiationDto;
    }

    public List<NegotiationMessageDto>  convertListMessagesToListMessagesDto(List<NegotiationMessage> lstMessages) {

	List<NegotiationMessageDto> lstMensajesDto = new ArrayList<NegotiationMessageDto>();

	for (NegotiationMessage mensaje : lstMessages) {
	    lstMensajesDto.add(messageConverter.TransformEntityToDto(mensaje));

	}
	return lstMensajesDto;
    };
    
    public List<NegotiationMessage> convertListMessagesDtoToListMessage(List<NegotiationMessageDto>lstMessagesDto){
	
	List<NegotiationMessage> lstMensajes = new ArrayList<NegotiationMessage>(); 
	for(NegotiationMessageDto mensaje:lstMessagesDto) {
	    lstMensajes.add(messageConverter.TransformDtoToEntity(mensaje));
	}
	return lstMensajes;
	
    }

}
