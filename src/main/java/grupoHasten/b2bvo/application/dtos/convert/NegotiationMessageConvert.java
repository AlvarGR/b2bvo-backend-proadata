package grupoHasten.b2bvo.application.dtos.convert;

import grupoHasten.b2bvo.application.dtos.NegotiationMessageDto;
import grupoHasten.b2bvo.application.model.NegotiationMessage;

public interface NegotiationMessageConvert {
    
    
    public NegotiationMessageDto TransformEntityToDto(NegotiationMessage negotiationMessage);
    public NegotiationMessage TransformDtoToEntity(NegotiationMessageDto negotiationMessageDto);    

}
