package grupoHasten.b2bvo.application.dtos.convert.impl;

import org.springframework.stereotype.Component;

import grupoHasten.b2bvo.application.dtos.TransportistaDto;
import grupoHasten.b2bvo.application.dtos.convert.TransportistaConvert;
import grupoHasten.b2bvo.application.model.Transportista;

@Component
public class TransportistaConvertImpl implements TransportistaConvert{

    public TransportistaDto convertEntityToDto(Transportista transportista){
	
	TransportistaDto newTransportistaDto = new TransportistaDto(); 
	
	newTransportistaDto.setId(transportista.getId());
	newTransportistaDto.setFullName(transportista.getFullName());
	newTransportistaDto.setOrganization(transportista.getOrganization());
	newTransportistaDto.setTelephonNumber(transportista.getTelephonNumber());
	
	return newTransportistaDto;
	
    }
    
    public Transportista convertDtoToEntity(TransportistaDto transportistaDto){
	
	Transportista transportista = new Transportista();
	
	transportista.setId(transportistaDto.getId());
	transportista.setFullName(transportista.getFullName());
	transportista.setOrganization(transportista.getOrganization());
	transportista.setTelephonNumber(transportista.getTelephonNumber());
	
	return transportista;
    }

}


