package grupoHasten.b2bvo.application.dtos.convert.impl;

import org.springframework.stereotype.Component;

import grupoHasten.b2bvo.application.dtos.ApplicationUserDto;
import grupoHasten.b2bvo.application.dtos.convert.ApplicationUserConvert;
import grupoHasten.b2bvo.application.model.ApplicationUser;

@Component
public class ApplicationUserConvertImpl implements ApplicationUserConvert{

    
    public ApplicationUserDto transformEntityToDto(ApplicationUser user) {
	
	ApplicationUserDto userDto = new ApplicationUserDto();
	
	userDto.setId(user.getId());
	userDto.setActive(user.isActive());
	userDto.setCompanyName(user.getCompanyName());
	userDto.setCountry(user.getCountry());
	userDto.setCreated_at(user.getCreated_at());
	userDto.setUpdated_at(user.getUpdated_at());
	userDto.setEmail(user.getEmail());
	userDto.setEmailVerified(user.isEmailVerified());
	userDto.setEnabled(user.isEnabled());
	userDto.setIs_tos_accepted(user.isIs_tos_accepted());
	userDto.setLastName(user.getLastName());
	userDto.setResetToken(user.getResetToken());
	userDto.setMunicipality(user.getMunicipality());
	userDto.setName(user.getName());
	userDto.setNational_id(user.getNational_id());
	userDto.setPassword(user.getPassword());
	userDto.setPosition(user.getPosition());
	userDto.setPostalCode(user.getPostalCode());
	userDto.setStreetName(user.getStreetName());
	userDto.setTaxId(user.getTaxId());
		
	return userDto;
    }
    
    public ApplicationUser transformDtoToEntity(ApplicationUserDto userDto) {
	
	ApplicationUser user = new ApplicationUser();
	
	if(userDto.getId()!=null)	user.setId(userDto.getId());
	user.setCountry(userDto.getCountry());
	user.setCreated_at(userDto.getCreated_at());
	user.setUpdated_at(userDto.getUpdated_at());
	user.setEmail(userDto.getEmail());
	user.setEmailVerified(userDto.isEmailVerified());
	user.setEnabled(userDto.isEnabled());
	user.setIs_tos_accepted(userDto.isIs_tos_accepted());
	user.setLastName(userDto.getLastName());
	user.setResetToken(userDto.getResetToken());
	user.setMunicipality(userDto.getMunicipality());
	user.setName(userDto.getName());
	user.setNational_id(userDto.getNational_id());
	user.setPassword(userDto.getPassword());
	user.setPosition(userDto.getPosition());
	user.setPostalCode(userDto.getPostalCode());
	user.setStreetName(userDto.getStreetName());
	user.setTaxId(userDto.getTaxId());
	 
	return user;
    }
}
