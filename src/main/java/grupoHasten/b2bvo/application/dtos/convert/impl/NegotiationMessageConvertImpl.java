package grupoHasten.b2bvo.application.dtos.convert.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import grupoHasten.b2bvo.application.dtos.NegotiationMessageDto;
import grupoHasten.b2bvo.application.dtos.convert.ApplicationUserConvert;
import grupoHasten.b2bvo.application.dtos.convert.NegotiationMessageConvert;
import grupoHasten.b2bvo.application.model.NegotiationMessage;
import grupoHasten.b2bvo.application.service.UserService;

@Component
public class NegotiationMessageConvertImpl implements NegotiationMessageConvert{
    
    @Autowired
    UserService userService;
    
    @Autowired
    ApplicationUserConvert userConvert;
    
    
    public NegotiationMessageDto TransformEntityToDto(NegotiationMessage negotiationMessage) {
	
	NegotiationMessageDto messageDto = new NegotiationMessageDto();
	
	messageDto.setId(negotiationMessage.getId());
	messageDto.setFechaEmision(negotiationMessage.getFechaEmision());
	messageDto.setMessage(negotiationMessage.getMessage());
	messageDto.setSender(userConvert.transformEntityToDto(negotiationMessage.getSender()));
	return messageDto;
    }
    
    public NegotiationMessage TransformDtoToEntity(NegotiationMessageDto negotiationMessageDto) {
	
	NegotiationMessage message = new NegotiationMessage();
	
	message.setId(negotiationMessageDto.getId());
	message.setFechaEmision(negotiationMessageDto.getFechaEmision());
	message.setMessage(negotiationMessageDto.getMessage());
	if(negotiationMessageDto.getSender()!=null) {
	    message.setSender(userService.findUserById(userConvert.transformDtoToEntity(negotiationMessageDto.getSender()).getId()).get());
	}
	return message;
    }

  

}
