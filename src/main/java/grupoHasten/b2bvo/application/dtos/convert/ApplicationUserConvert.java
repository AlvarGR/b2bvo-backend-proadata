package grupoHasten.b2bvo.application.dtos.convert;

import grupoHasten.b2bvo.application.dtos.ApplicationUserDto;
import grupoHasten.b2bvo.application.model.ApplicationUser;

public interface ApplicationUserConvert {
    
    public ApplicationUserDto transformEntityToDto(ApplicationUser user);

    public ApplicationUser transformDtoToEntity(ApplicationUserDto userDto); 
}
