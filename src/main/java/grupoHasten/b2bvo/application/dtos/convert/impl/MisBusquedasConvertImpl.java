package grupoHasten.b2bvo.application.dtos.convert.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import grupoHasten.b2bvo.application.dtos.MisBusquedasDto;
import grupoHasten.b2bvo.application.dtos.SearchCriteriaDto;
import grupoHasten.b2bvo.application.dtos.convert.MisBusquedasConvert;
import grupoHasten.b2bvo.application.model.ApplicationUser;
import grupoHasten.b2bvo.application.model.MisBusquedas;
import grupoHasten.b2bvo.application.repository.IUserRepository;

@Component
public class MisBusquedasConvertImpl implements MisBusquedasConvert {

	@Autowired
	IUserRepository userRepository;
	private ObjectMapper mapper;
	
	@PostConstruct
	public void init(){
	   mapper = new ObjectMapper();
	   mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
	}
	
	@Override
	public MisBusquedas transformDtoToEntity(MisBusquedasDto misBusquedasDto) {
		MisBusquedas misBusquedas = new MisBusquedas();
		misBusquedas.setId(misBusquedasDto.getId());
		misBusquedas.setUser(obtenerUser(misBusquedasDto.getUser()));
		misBusquedas.setName(misBusquedasDto.getName());
		String jsonStr = null;
		try {
			jsonStr = mapper.writeValueAsString(misBusquedasDto.getSearch());
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		misBusquedas.setSearch(jsonStr);
		return misBusquedas;
	}

	@Override
	public MisBusquedasDto transformEntityToDto(MisBusquedas misBusquedas) {
		
		MisBusquedasDto misBusquedasDto = new MisBusquedasDto();
		misBusquedasDto.setId(misBusquedas.getId());
		misBusquedasDto.setUser(misBusquedas.getUser().getName());
		misBusquedasDto.setName(misBusquedas.getName());
		SearchCriteriaDto search = null;
		try {
			search = mapper.readValue(misBusquedas.getSearch(), SearchCriteriaDto.class);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		misBusquedasDto.setSearch(search);
		return misBusquedasDto;
	}

	@Override
	public List<MisBusquedasDto> transformListEntitiesToListDto(List<MisBusquedas> misBusquedas) {
		List<MisBusquedasDto> misBusquedasListDto = new ArrayList<>();
		for (MisBusquedas item: misBusquedas) {
			misBusquedasListDto.add(transformEntityToDto(item));
		}
		return misBusquedasListDto;
	}
	
	public ApplicationUser obtenerUser(String userName) {
		ApplicationUser user = userRepository.findByName(userName);
		return user;
	}

}
