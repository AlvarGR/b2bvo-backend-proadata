package grupoHasten.b2bvo.application.dtos.convert;

import grupoHasten.b2bvo.application.dtos.TransportistaDto;
import grupoHasten.b2bvo.application.model.Transportista;

public interface TransportistaConvert {
    
    public TransportistaDto convertEntityToDto(Transportista transportista);
    
    public Transportista convertDtoToEntity(TransportistaDto transportistaDto);

}
