package grupoHasten.b2bvo.application.dtos.convert;

import org.springframework.data.domain.Page;

import grupoHasten.b2bvo.application.dtos.VehicleDto;
import grupoHasten.b2bvo.application.model.Vehicle;

import java.util.List;

public interface VehicleConvert {
	Vehicle transformDtoToEntity(VehicleDto vehicleDto);
	VehicleDto transformEntityToDto(Vehicle vehicle);
	List<VehicleDto> transformListEntitiesToListDto(List<Vehicle> vehicles);
	Page<VehicleDto> pageEntitiesToPageDto(Page<Vehicle> vehicles);

}
