package grupoHasten.b2bvo.application.dtos.convert;

import grupoHasten.b2bvo.application.dtos.NegotiationDto;
import grupoHasten.b2bvo.application.model.Negotiation;

public interface NegotiationConvert {

    Negotiation transformDtoToEntity(NegotiationDto negotiationDto);

    NegotiationDto transformEntityToDto(Negotiation negotiation);
}
