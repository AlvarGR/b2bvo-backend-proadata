package grupoHasten.b2bvo.application.dtos.convert.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import grupoHasten.b2bvo.application.dtos.ImporteDto;
import grupoHasten.b2bvo.application.dtos.LoteDto;
import grupoHasten.b2bvo.application.dtos.VehicleDto;
import grupoHasten.b2bvo.application.dtos.convert.LoteConvert;
import grupoHasten.b2bvo.application.dtos.convert.VehicleConvert;
import grupoHasten.b2bvo.application.model.ApplicationUser;
import grupoHasten.b2bvo.application.model.Lote;
import grupoHasten.b2bvo.application.model.Vehicle;
import grupoHasten.b2bvo.application.repository.IUserRepository;
import grupoHasten.b2bvo.application.repository.VehicleRepository;
import grupoHasten.b2bvo.application.service.ImporteService;

@Component
public class LoteConvertImpl implements LoteConvert {

  @Autowired
  VehicleConvert vehicleConvert;

  @Autowired
  VehicleRepository vehicleRepository;

  @Autowired
  ImporteService importeService;

  @Autowired
  IUserRepository userRepository;

  @Override
  public Lote transformDtoToEntity(LoteDto loteDto) {

    Lote lote = new Lote();

    loteDto.setPrice(importeService.calcularImporte(loteDto.getPrice()));

    lote.setId(loteDto.getId());
    lote.setName(loteDto.getName());
    rellenaImporte(lote, loteDto);
    lote.setAvailabilityDate(loteDto.getAvailabilityDate());
    lote.setPublicationDate(loteDto.getPublicationDate());
    lote.setDeliveryPlace(loteDto.getDeliveryPlace());
    lote.setContactPerson(obtenerSeller(loteDto.getContactPerson()));
    return lote;
  }

  @Override
  public LoteDto transformEntityToDto(Lote lote) {
    return LoteDto.builder().availabilityDate(lote.getAvailabilityDate())
        .contactPerson(lote.getContactPerson().getName()).deliveryPlace(lote.getDeliveryPlace())
        .id(lote.getId()).name(lote.getName()).vehiclesDto(buildVehiclesDto(lote.getVehicles()))
        .nameContactPerson(lote.getContactPerson().getLastName())
        .organizacionDto(lote.getOrganization()).price(rellenaImporteDto(lote))
        .publicationDate(lote.getPublicationDate()).build();
  }

  @Override
  public Page<LoteDto> transformEntityToDtoLst(Page<Lote> lote) {
    return lote.map(this::transformEntityToDto);
  }

  public Lote actualizarEntity(Lote lote, LoteDto loteDto) {
    loteDto.setPrice(importeService.calcularImporte(loteDto.getPrice()));
    lote.setName(loteDto.getName());
    rellenaImporte(lote, loteDto);
    lote.setPublicationDate(loteDto.getPublicationDate());
    lote.setAvailabilityDate(loteDto.getAvailabilityDate());
    lote.setDeliveryPlace(loteDto.getDeliveryPlace());
    lote.setContactPerson(obtenerSeller(loteDto.getContactPerson()));

    return lote;
  }


  private void rellenaImporte(Lote lote, LoteDto loteDto) {
    lote.setBasePrice(loteDto.getPrice().getPrecioBase());
    lote.setFinalPrice(loteDto.getPrice().getPrecioFinal());
    lote.setDiscountAmount(loteDto.getPrice().getDescuentoCantidad());
    lote.setDiscountPercentage(loteDto.getPrice().getDescuentoPorcentaje());

  }

  private ImporteDto rellenaImporteDto(Lote lote) {
    ImporteDto importeDto = new ImporteDto();

    importeDto.setPrecioBase(lote.getBasePrice());
    importeDto.setPrecioFinal(lote.getFinalPrice());
    importeDto.setDescuentoCantidad(lote.getDiscountAmount());
    importeDto.setDescuentoPorcentaje(lote.getDiscountPercentage());

    return importeDto;

  }

  private List<VehicleDto> buildVehiclesDto(List<Vehicle> vehicles) {
    return vehicles.stream().map(v -> {
      return vehicleConvert.transformEntityToDto(v);
    }).collect(Collectors.toList());
  }

  private ApplicationUser obtenerSeller(String seller) {
    ApplicationUser applicationUser = userRepository.findByName(seller);
    return applicationUser;
  }



}
