package grupoHasten.b2bvo.application.dtos;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.validation.constraints.NotNull;

import grupoHasten.b2bvo.application.model.NegotiationDocument;

public class NegotiationDto implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private Long id;

    private String identification;

    private Date fechaInicio;

    private Date fechaUltimoMensaje;

    private String/*NegotiationState*/ state;

    private OrganizationDto comprador;

    private OrganizationDto vendedor;

    @NotNull
    private ApplicationUserDto contactoVendedor;

    @NotNull
    private ApplicationUserDto contactoComprador;

    private TransportistaDto transportistaDto;

    private List<VehicleDto> lstVehiclesDto;
    
    private List<LoteDto> lstLotesDto;
    
    private List<NegotiationMessageDto> messages;
    
    private Set<NegotiationDocument> documents;

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getIdentification() {
	return identification;
    }

    public void setIdentification(String identification) {
	this.identification = identification;
    }

    public Date getFechaInicio() {
	return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
	this.fechaInicio = fechaInicio;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public OrganizationDto getComprador() {
        return comprador;
    }

    public void setComprador(OrganizationDto comprador) {
        this.comprador = comprador;
    }

    public OrganizationDto getVendedor() {
        return vendedor;
    }

    public void setVendedor(OrganizationDto vendedor) {
        this.vendedor = vendedor;
    }

    public ApplicationUserDto getContactoVendedor() {
        return contactoVendedor;
    }

    public void setContactoVendedor(ApplicationUserDto contactoVendedor) {
        this.contactoVendedor = contactoVendedor;
    }

    public ApplicationUserDto getContactoComprador() {
        return contactoComprador;
    }

    public void setContactoComprador(ApplicationUserDto contactoComprador) {
        this.contactoComprador = contactoComprador;
    }

    public TransportistaDto getTransportistaDto() {
        return transportistaDto;
    }

    public void setTransportistaDto(TransportistaDto transportistaDto) {
        this.transportistaDto = transportistaDto;
    }

    public List<VehicleDto> getLstVehiclesDto() {
        return lstVehiclesDto;
    }

    public void setLstVehiclesDto(List<VehicleDto> lstVehiclesDto) {
        this.lstVehiclesDto = lstVehiclesDto;
    }

    public List<LoteDto> getLstLotesDto() {
        return lstLotesDto;
    }

    public void setLstLotesDto(List<LoteDto> lstLotesDto) {
        this.lstLotesDto = lstLotesDto;
    }

    public List<NegotiationMessageDto> getMessages() {
        return messages;
    }

    public void setMessages(List<NegotiationMessageDto> messages) {
        this.messages = messages;
    }

    public Set<NegotiationDocument> getDocuments() {
        return documents;
    }

    public void setDocuments(Set<NegotiationDocument> documents) {
        this.documents = documents;
    }

    public Date getFechaUltimoMensaje() {
	return fechaUltimoMensaje;
    }

    public void setFechaUltimoMensaje(Date fechaUltimoMensaje) {
	this.fechaUltimoMensaje = fechaUltimoMensaje;
    }

 
}
