package grupoHasten.b2bvo.application.dtos;

import java.io.Serializable;

@SuppressWarnings("serial")
public class ResponseBuckerDto implements Serializable {
	private String status;
	private String key;
	
	public ResponseBuckerDto() {}
	
	public ResponseBuckerDto(String status, String key) {
		super();
		this.status = status;
		this.key = key;
	}

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	
	
}
