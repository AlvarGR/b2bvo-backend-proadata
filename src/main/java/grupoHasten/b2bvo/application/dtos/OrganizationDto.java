package grupoHasten.b2bvo.application.dtos;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.NotNull;

import grupoHasten.b2bvo.application.model.Address;

public class OrganizationDto {
    
    @NotNull
    private Long id;
    
    @NotNull
    private Long parentId;
    
    @NotNull
    private String taxId;
    
    @NotNull
    private String companyName;
    
    @NotNull
    private Boolean verified;
    
    @NotNull
    private BigDecimal verificationCost;
    
    @NotNull
    private Set<Address> orgAddresses = new HashSet<Address>();

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getTaxId() {
        return taxId;
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Boolean getVerified() {
        return verified;
    }

    public void setVerified(Boolean verified) {
        this.verified = verified;
    }

    public BigDecimal getVerificationCost() {
        return verificationCost;
    }

    public void setVerificationCost(BigDecimal verificationCost) {
        this.verificationCost = verificationCost;
    }

    public Set<Address> getOrgAddresses() {
        return orgAddresses;
    }

    public void setOrgAddresses(Set<Address> orgAddresses) {
        this.orgAddresses = orgAddresses;
    }
   
}
