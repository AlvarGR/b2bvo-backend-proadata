package grupoHasten.b2bvo.application.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@SuppressWarnings("serial")
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AttachmentDto implements Serializable {
  private String type;
  private String url;

}
