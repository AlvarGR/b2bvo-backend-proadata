package grupoHasten.b2bvo.application.dtos;

import javax.validation.constraints.NotNull;

import grupoHasten.b2bvo.application.model.Organization;

public class TransportistaDto {

    	
    private Long id;

    @NotNull
    private Organization organization;

    @NotNull
    private String fullName;

    @NotNull
    private String telephonNumber;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getTelephonNumber() {
        return telephonNumber;
    }

    public void setTelephonNumber(String telephonNumber) {
        this.telephonNumber = telephonNumber;
    }
    
    

}
