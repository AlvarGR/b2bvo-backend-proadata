package grupoHasten.b2bvo.application.dtos;

public class ImporteDto {
	private Double precioBase;
	private Double descuentoPorcentaje;
	private Double descuentoCantidad;
	private Double precioFinal;
	
	public Double getPrecioBase() {
		return precioBase;
	}
	public void setPrecioBase(Double precioBase) {
		this.precioBase = precioBase;
	}
	public Double getDescuentoPorcentaje() {
		return descuentoPorcentaje;
	}
	public void setDescuentoPorcentaje(Double descuentoPorcentaje) {
		this.descuentoPorcentaje = descuentoPorcentaje;
	}
	public Double getDescuentoCantidad() {
		return descuentoCantidad;
	}
	public void setDescuentoCantidad(Double descuentoCantidad) {
		this.descuentoCantidad = descuentoCantidad;
	}
	public Double getPrecioFinal() {
		return precioFinal;
	}
	public void setPrecioFinal(Double precioFinal) {
		this.precioFinal = precioFinal;
	}
	
	
}
