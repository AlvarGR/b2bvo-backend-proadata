
package grupoHasten.b2bvo.application.facade.model;

public class JatoModel {

    private String model;
    private String localModel;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getLocalModel() {
        return localModel;
    }

    public void setLocalModel(String localModel) {
        this.localModel = localModel;
    }

}
