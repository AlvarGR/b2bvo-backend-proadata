
package grupoHasten.b2bvo.application.facade.model;

public class JatoMake {

    private String make;
    private String localMake;

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getLocalMake() {
        return localMake;
    }

    public void setLocalMake(String localMake) {
        this.localMake = localMake;
    }

}
