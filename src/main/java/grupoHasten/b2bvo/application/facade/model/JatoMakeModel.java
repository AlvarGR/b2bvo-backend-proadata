package grupoHasten.b2bvo.application.facade.model;

import java.util.ArrayList;
import java.util.List;

public class JatoMakeModel {
    private String make;
    private String localMake;
    List<JatoModel> models;
    
    public JatoMakeModel() {
		super();
		models = new ArrayList<>();
	}

	public void addModel(JatoModel model) {
    	if (models == null) {
    		models = new ArrayList<>();
    	}
    	
    	models.add(model);
    }
	
	public void removeModel(JatoModel model) {
		models.remove(model);
	}
    
	public String getMake() {
		return make;
	}
	public void setMake(String make) {
		this.make = make;
	}
	public String getLocalMake() {
		return localMake;
	}
	public void setLocalMake(String localMake) {
		this.localMake = localMake;
	}
	public List<JatoModel> getModels() {
		return models;
	}
	public void setModels(List<JatoModel> models) {
		this.models = models;
	}
    
    
}
