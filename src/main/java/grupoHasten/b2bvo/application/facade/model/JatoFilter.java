
package grupoHasten.b2bvo.application.facade.model;

import java.util.List;

public class JatoFilter {

    private List<String> modelYears = null;
    private List<String> numberOfDoors = null;
    private List<BodyStyle> bodyStyles = null;
    private List<FuelType> fuelTypes = null;
    private List<TransmissionType> transmissionTypes = null;
    private List<String> engineLitres = null;

    public List<String> getModelYears() {
        return modelYears;
    }

    public void setModelYears(List<String> modelYears) {
        this.modelYears = modelYears;
    }

    public List<String> getNumberOfDoors() {
        return numberOfDoors;
    }

    public void setNumberOfDoors(List<String> numberOfDoors) {
        this.numberOfDoors = numberOfDoors;
    }

    public List<BodyStyle> getBodyStyles() {
        return bodyStyles;
    }

    public void setBodyStyles(List<BodyStyle> bodyStyles) {
        this.bodyStyles = bodyStyles;
    }

    public List<FuelType> getFuelTypes() {
        return fuelTypes;
    }

    public void setFuelTypes(List<FuelType> fuelTypes) {
        this.fuelTypes = fuelTypes;
    }

    public List<TransmissionType> getTransmissionTypes() {
        return transmissionTypes;
    }

    public void setTransmissionTypes(List<TransmissionType> transmissionTypes) {
        this.transmissionTypes = transmissionTypes;
    }

    public List<String> getEngineLitres() {
        return engineLitres;
    }

    public void setEngineLitres(List<String> engineLitres) {
        this.engineLitres = engineLitres;
    }

}
