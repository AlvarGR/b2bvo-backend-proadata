
package grupoHasten.b2bvo.application.facade.model;

public class JatoVersion {

    private Long vehicleId;
    private String make;
    private String localMake;
    private String model;
    private String localModel;
    private String versionName;
    private String localVersionName;
    private String manufacturerCode;
    private String modelYear;
    private String numberOfDoors;
    private BodyStyle bodyStyle;
    private FuelType fuelType;
    private TransmissionType transmissionType;
    private String engineLitres;
    private String versionStartDate;
    private String versionConcludeDate;
    private Double basePrice;
    private Double sellPrice;
    private String priceCurrency;

    public Long getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(Long vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getLocalMake() {
        return localMake;
    }

    public void setLocalMake(String localMake) {
        this.localMake = localMake;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getLocalModel() {
        return localModel;
    }

    public void setLocalModel(String localModel) {
        this.localModel = localModel;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public String getLocalVersionName() {
        return localVersionName;
    }

    public void setLocalVersionName(String localVersionName) {
        this.localVersionName = localVersionName;
    }

    public String getManufacturerCode() {
        return manufacturerCode;
    }

    public void setManufacturerCode(String manufacturerCode) {
        this.manufacturerCode = manufacturerCode;
    }

    public String getModelYear() {
        return modelYear;
    }

    public void setModelYear(String modelYear) {
        this.modelYear = modelYear;
    }

    public String getNumberOfDoors() {
        return numberOfDoors;
    }

    public void setNumberOfDoors(String numberOfDoors) {
        this.numberOfDoors = numberOfDoors;
    }

    public BodyStyle getBodyStyle() {
        return bodyStyle;
    }

    public void setBodyStyle(BodyStyle bodyStyle) {
        this.bodyStyle = bodyStyle;
    }

    public FuelType getFuelType() {
        return fuelType;
    }

    public void setFuelType(FuelType fuelType) {
        this.fuelType = fuelType;
    }

    public TransmissionType getTransmissionType() {
        return transmissionType;
    }

    public void setTransmissionType(TransmissionType transmissionType) {
        this.transmissionType = transmissionType;
    }

    public String getEngineLitres() {
        return engineLitres;
    }

    public void setEngineLitres(String engineLitres) {
        this.engineLitres = engineLitres;
    }

    public String getVersionStartDate() {
        return versionStartDate;
    }

    public void setVersionStartDate(String versionStartDate) {
        this.versionStartDate = versionStartDate;
    }

    public String getVersionConcludeDate() {
        return versionConcludeDate;
    }

    public void setVersionConcludeDate(String versionConcludeDate) {
        this.versionConcludeDate = versionConcludeDate;
    }

    public Double getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(Double basePrice) {
        this.basePrice = basePrice;
    }

    public Double getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(Double sellPrice) {
        this.sellPrice = sellPrice;
    }

    public String getPriceCurrency() {
        return priceCurrency;
    }

    public void setPriceCurrency(String priceCurrency) {
        this.priceCurrency = priceCurrency;
    }

}
