
package grupoHasten.b2bvo.application.facade.model;

import java.util.HashMap;
import java.util.Map;

public class ExternalDimensions {

    private Long length;
    private Long width;
    private Long height;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Long getLength() {
        return length;
    }

    public void setLength(Long length) {
        this.length = length;
    }

    public Long getWidth() {
        return width;
    }

    public void setWidth(Long width) {
        this.width = width;
    }

    public Long getHeight() {
        return height;
    }

    public void setHeight(Long height) {
        this.height = height;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
