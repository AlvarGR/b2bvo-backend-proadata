
package grupoHasten.b2bvo.application.facade.model;

import java.util.HashMap;
import java.util.Map;

public class FuelConsumption {

    private Double urban;
    private Double combined;
    private Double highway;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Double getUrban() {
        return urban;
    }

    public void setUrban(Double urban) {
        this.urban = urban;
    }

    public Double getCombined() {
        return combined;
    }

    public void setCombined(Double combined) {
        this.combined = combined;
    }

    public Double getHighway() {
        return highway;
    }

    public void setHighway(Double highway) {
        this.highway = highway;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
