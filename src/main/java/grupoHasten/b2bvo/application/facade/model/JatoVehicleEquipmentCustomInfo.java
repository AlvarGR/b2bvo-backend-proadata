
package grupoHasten.b2bvo.application.facade.model;

import java.util.HashMap;
import java.util.Map;

public class JatoVehicleEquipmentCustomInfo {

    private ExternalDimensions externalDimensions;
    private FuelConsumption fuelConsumption;
    private Object co2Emission;
    private Double fuelTankCapacity;
    private Long seatingCapacity;
    private Object versionId;
    private String specialEdition;
    private String drivenWheels;
    private String manufacturerCode;
    private Long engineCC;
    private Long numberOfCylinders;
    private Long maximumPowerKw;
    private Long maximumPowerCv;
    private Double litres;
    private Object compressor;
    private Long frontTiresWidth;
    private Long rearTiresWidth;
    private Long frontTiresProfile;
    private Long rearTiresProfile;
    private String localTrimLevel;
    private String trimLevel;
    private Object trimClassification;
    private Long vehicleId;
    private String make;
    private String localMake;
    private String model;
    private String localModel;
    private String versionName;
    private String localVersionName;
    private String modelYear;
    private String numberOfDoors;
    private BodyStyle bodyStyle;
    private FuelType fuelType;
    private TransmissionType transmissionType;
    private String engineLitres;
    private String versionStartDate;
    private String versionConcludeDate;
    private Double basePrice;
    private Double sellPrice;
    private String priceCurrency;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public ExternalDimensions getExternalDimensions() {
        return externalDimensions;
    }

    public void setExternalDimensions(ExternalDimensions externalDimensions) {
        this.externalDimensions = externalDimensions;
    }

    public FuelConsumption getFuelConsumption() {
        return fuelConsumption;
    }

    public void setFuelConsumption(FuelConsumption fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    public Object getCo2Emission() {
        return co2Emission;
    }

    public void setCo2Emission(Object co2Emission) {
        this.co2Emission = co2Emission;
    }

    public Double getFuelTankCapacity() {
        return fuelTankCapacity;
    }

    public void setFuelTankCapacity(Double fuelTankCapacity) {
        this.fuelTankCapacity = fuelTankCapacity;
    }

    public Long getSeatingCapacity() {
        return seatingCapacity;
    }

    public void setSeatingCapacity(Long seatingCapacity) {
        this.seatingCapacity = seatingCapacity;
    }

    public Object getVersionId() {
        return versionId;
    }

    public void setVersionId(Object versionId) {
        this.versionId = versionId;
    }

    public String getSpecialEdition() {
        return specialEdition;
    }

    public void setSpecialEdition(String specialEdition) {
        this.specialEdition = specialEdition;
    }

    public String getDrivenWheels() {
        return drivenWheels;
    }

    public void setDrivenWheels(String drivenWheels) {
        this.drivenWheels = drivenWheels;
    }

    public String getManufacturerCode() {
        return manufacturerCode;
    }

    public void setManufacturerCode(String manufacturerCode) {
        this.manufacturerCode = manufacturerCode;
    }

    public Long getEngineCC() {
        return engineCC;
    }

    public void setEngineCC(Long engineCC) {
        this.engineCC = engineCC;
    }

    public Long getNumberOfCylinders() {
        return numberOfCylinders;
    }

    public void setNumberOfCylinders(Long numberOfCylinders) {
        this.numberOfCylinders = numberOfCylinders;
    }

    public Long getMaximumPowerKw() {
        return maximumPowerKw;
    }

    public void setMaximumPowerKw(Long maximumPowerKw) {
        this.maximumPowerKw = maximumPowerKw;
    }

    public Long getMaximumPowerCv() {
        return maximumPowerCv;
    }

    public void setMaximumPowerCv(Long maximumPowerCv) {
        this.maximumPowerCv = maximumPowerCv;
    }

    public Double getLitres() {
        return litres;
    }

    public void setLitres(Double litres) {
        this.litres = litres;
    }

    public Object getCompressor() {
        return compressor;
    }

    public void setCompressor(Object compressor) {
        this.compressor = compressor;
    }

    public Long getFrontTiresWidth() {
        return frontTiresWidth;
    }

    public void setFrontTiresWidth(Long frontTiresWidth) {
        this.frontTiresWidth = frontTiresWidth;
    }

    public Long getRearTiresWidth() {
        return rearTiresWidth;
    }

    public void setRearTiresWidth(Long rearTiresWidth) {
        this.rearTiresWidth = rearTiresWidth;
    }

    public Long getFrontTiresProfile() {
        return frontTiresProfile;
    }

    public void setFrontTiresProfile(Long frontTiresProfile) {
        this.frontTiresProfile = frontTiresProfile;
    }

    public Long getRearTiresProfile() {
        return rearTiresProfile;
    }

    public void setRearTiresProfile(Long rearTiresProfile) {
        this.rearTiresProfile = rearTiresProfile;
    }

    public String getLocalTrimLevel() {
        return localTrimLevel;
    }

    public void setLocalTrimLevel(String localTrimLevel) {
        this.localTrimLevel = localTrimLevel;
    }

    public String getTrimLevel() {
        return trimLevel;
    }

    public void setTrimLevel(String trimLevel) {
        this.trimLevel = trimLevel;
    }

    public Object getTrimClassification() {
        return trimClassification;
    }

    public void setTrimClassification(Object trimClassification) {
        this.trimClassification = trimClassification;
    }

    public Long getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(Long vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getLocalMake() {
        return localMake;
    }

    public void setLocalMake(String localMake) {
        this.localMake = localMake;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getLocalModel() {
        return localModel;
    }

    public void setLocalModel(String localModel) {
        this.localModel = localModel;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public String getLocalVersionName() {
        return localVersionName;
    }

    public void setLocalVersionName(String localVersionName) {
        this.localVersionName = localVersionName;
    }

    public String getModelYear() {
        return modelYear;
    }

    public void setModelYear(String modelYear) {
        this.modelYear = modelYear;
    }

    public String getNumberOfDoors() {
        return numberOfDoors;
    }

    public void setNumberOfDoors(String numberOfDoors) {
        this.numberOfDoors = numberOfDoors;
    }

    public BodyStyle getBodyStyle() {
        return bodyStyle;
    }

    public void setBodyStyle(BodyStyle bodyStyle) {
        this.bodyStyle = bodyStyle;
    }

    public FuelType getFuelType() {
        return fuelType;
    }

    public void setFuelType(FuelType fuelType) {
        this.fuelType = fuelType;
    }

    public TransmissionType getTransmissionType() {
        return transmissionType;
    }

    public void setTransmissionType(TransmissionType transmissionType) {
        this.transmissionType = transmissionType;
    }

    public String getEngineLitres() {
        return engineLitres;
    }

    public void setEngineLitres(String engineLitres) {
        this.engineLitres = engineLitres;
    }

    public String getVersionStartDate() {
        return versionStartDate;
    }

    public void setVersionStartDate(String versionStartDate) {
        this.versionStartDate = versionStartDate;
    }

    public String getVersionConcludeDate() {
        return versionConcludeDate;
    }

    public void setVersionConcludeDate(String versionConcludeDate) {
        this.versionConcludeDate = versionConcludeDate;
    }

    public Double getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(Double basePrice) {
        this.basePrice = basePrice;
    }

    public Double getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(Double sellPrice) {
        this.sellPrice = sellPrice;
    }

    public String getPriceCurrency() {
        return priceCurrency;
    }

    public void setPriceCurrency(String priceCurrency) {
        this.priceCurrency = priceCurrency;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
