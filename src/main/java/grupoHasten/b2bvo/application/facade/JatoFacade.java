package grupoHasten.b2bvo.application.facade;

import java.util.List;

import grupoHasten.b2bvo.application.facade.model.JatoFilter;
import grupoHasten.b2bvo.application.facade.model.JatoMake;
import grupoHasten.b2bvo.application.facade.model.JatoModel;
import grupoHasten.b2bvo.application.facade.model.JatoVehicleEquipmentCustomInfo;
import grupoHasten.b2bvo.application.facade.model.JatoVersion;

public interface JatoFacade {
	List<JatoMake> obtenerMakes();
	List<JatoModel> obtenerModels(String make);
	JatoFilter obtenerFilter(String make, String model);
	List<JatoVersion> obtenerVersion(String make, String model);
	JatoVehicleEquipmentCustomInfo obtenerJatoVehicleEquipmentCustomInfo(String version);
}
