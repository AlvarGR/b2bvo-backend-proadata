package grupoHasten.b2bvo.application.facade.impl;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import grupoHasten.b2bvo.application.facade.JatoFacade;
import grupoHasten.b2bvo.application.facade.model.JatoFilter;
import grupoHasten.b2bvo.application.facade.model.JatoMake;
import grupoHasten.b2bvo.application.facade.model.JatoModel;
import grupoHasten.b2bvo.application.facade.model.JatoVehicleEquipmentCustomInfo;
import grupoHasten.b2bvo.application.facade.model.JatoVersion;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
public class JatoFacadeImpl implements JatoFacade {

  final String uriBase = "https://efe8mlzotj.execute-api.eu-west-1.amazonaws.com";
  final String makeEndPoint = "/api/makes";
  final String modelEndPoint = "/api/models";
  final String filterEndPoint = "/api/filters";
  final String versionEndPoint = "/api/versions";
  final String vehicleCustomInfoEndPoint = "/api/vehicle/{version}/equipment/custom-info";

  @Value("${jato.xapikey.value}")
  private String xApiKeyValue;

  public List<JatoMake> obtenerMakes() {
    log.info("obtenerMakes");
    RestTemplate restTemplate = new RestTemplate();

    HttpHeaders headers = new HttpHeaders();
    headers.add("X-Api-Key", xApiKeyValue);

    HttpEntity<String> request = new HttpEntity<String>(headers);

    ResponseEntity<JatoMake[]> response =
        restTemplate.exchange(uriBase + makeEndPoint, HttpMethod.GET, request, JatoMake[].class);
    JatoMake[] makes = response.getBody();
    return Arrays.asList(makes);

  }

  public List<JatoModel> obtenerModels(String make) {
    log.info("obtenerModels");

    RestTemplate restTemplate = new RestTemplate();

    HttpHeaders headers = new HttpHeaders();
    headers.add("X-Api-Key", xApiKeyValue);

    // Query parameters
    UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(uriBase + modelEndPoint)
        // Add query parameter
        .queryParam("make", make);

    HttpEntity<String> request = new HttpEntity<String>(headers);

    ResponseEntity<JatoModel[]> response = restTemplate.exchange(builder.buildAndExpand().toUri(),
        HttpMethod.GET, request, JatoModel[].class);
    JatoModel[] models = response.getBody();
    return Arrays.asList(models);
  }

  @Override
  public JatoFilter obtenerFilter(String make, String model) {
    log.info("obtenerFilter");
    RestTemplate restTemplate = new RestTemplate();

    HttpHeaders headers = new HttpHeaders();
    headers.add("X-Api-Key", xApiKeyValue);

    // Query parameters
    UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(uriBase + filterEndPoint)
        // Add query parameter
        .queryParam("make", make).queryParam("model", model);

    System.out.println(builder.buildAndExpand().toUri());
    HttpEntity<String> request = new HttpEntity<String>(headers);

    ResponseEntity<JatoFilter> response = restTemplate.exchange(builder.buildAndExpand().toUri(),
        HttpMethod.GET, request, JatoFilter.class);
    JatoFilter filter = response.getBody();
    return filter;
  }

  public List<JatoVersion> obtenerVersion(String make, String model) {
    log.info("obtenerVersion: {}, {}", make, model);
    RestTemplate restTemplate = new RestTemplate();

    HttpHeaders headers = new HttpHeaders();
    headers.add("X-Api-Key", xApiKeyValue);

    // Query parameters
    UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(uriBase + versionEndPoint)
        // Add query parameter
        .queryParam("make", make).queryParam("model", model);

    HttpEntity<String> request = new HttpEntity<String>(headers);

    ResponseEntity<JatoVersion[]> response = restTemplate.exchange(builder.buildAndExpand().toUri(),
        HttpMethod.GET, request, JatoVersion[].class);
    JatoVersion[] versiones = response.getBody();
    log.info("Fin obtenerVersion {}, {}", make, model);
    return Arrays.asList(versiones);
  }

  @Override
  public JatoVehicleEquipmentCustomInfo obtenerJatoVehicleEquipmentCustomInfo(String version) {
    RestTemplate restTemplate = new RestTemplate();

    HttpHeaders headers = new HttpHeaders();
    headers.add("X-Api-Key", xApiKeyValue);

    Map<String, String> params = new HashMap<String, String>();
    params.put("version", version);

    UriComponentsBuilder builder =
        UriComponentsBuilder.fromUriString(uriBase + vehicleCustomInfoEndPoint);

    HttpEntity<String> request = new HttpEntity<String>(headers);

    System.out.println(builder.buildAndExpand(params).toUri());
    ResponseEntity<JatoVehicleEquipmentCustomInfo> response =
        restTemplate.exchange(builder.buildAndExpand(params).toUri(), HttpMethod.GET, request,
            JatoVehicleEquipmentCustomInfo.class);
    JatoVehicleEquipmentCustomInfo customInfo = response.getBody();
    return customInfo;

  }
}
