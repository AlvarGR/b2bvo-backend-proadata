package grupoHasten.b2bvo.application.enumerates;

public enum VehicleState {
	
	NUEVO("Nuevo", "Nuevo"),
	SEGUNDA_MANO("SegundaMano", "Segunda Mano"),
	KM0("KM0", "KM 0"),
	SEMINUEVO("Seminuevo", "Seminuevo");
	
    private String key;
    private String state;
    
	private VehicleState(String key, String state) {
		this.key = key;
		this.state = state;
	}

	
	static public VehicleState obtenerVehicleState(String state) {
		
		VehicleState[] vehicles = VehicleState.values();
		for (VehicleState vehicle: vehicles) {
			if (vehicle.getState().equals(state)) {
				return vehicle;
			}
		}
		return null;
	}
	
	static public VehicleState obtenerState(String key) {
		
		VehicleState[] vehicles = VehicleState.values();
		for (VehicleState vehicle: vehicles) {
			if (vehicle.getKey().equals(key)) {
				return vehicle;
			}
		}
		return null;
	}
	
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
    
}
