package grupoHasten.b2bvo.application.enumerates;

public enum DocumentClass {

	Reservation(0L, "Reserva"),
	PurchaseContract(1L, "Contrato Compra/Venta"),
	Management(2L, "Gestion"),
	Other(3L, "Otros Clase Documentos");
	
	private Long key;
	private String clase;
	
	DocumentClass(Long key, String clase) {
		this.key = key;
		this.clase = clase;
	}

	public static DocumentClass getByKey(Long key) {
		DocumentClass[] clases = DocumentClass.values();
		for (DocumentClass item: clases) {
			if (item.getKey().equals(key)) {
				return item;
			}
		}
		return null;
	}
	
	public static DocumentClass getByClase(String clase) {
		DocumentClass[] clases = DocumentClass.values();
		for (DocumentClass item: clases) {
			if (item.getClase().equals(clase)) {
				return item;
			}
		}
		return null;
	}
	
	public Long getKey() {
		return key;
	}

	public void setKey(Long key) {
		this.key = key;
	}

	public String getClase() {
		return clase;
	}

	public void setClase(String clase) {
		this.clase = clase;
	}
	
}
