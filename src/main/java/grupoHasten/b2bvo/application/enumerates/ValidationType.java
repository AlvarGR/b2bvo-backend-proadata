package grupoHasten.b2bvo.application.enumerates;

public enum ValidationType {
	
	Sender(0L, "Enviador"),
	Receiver(1L, "Receptor"),
	Both(2L, "Ambos");
	
	private Long key;
	private String type;
	
	ValidationType(Long key, String type) {
		this.key = key;
		this.type = type;
	}
	
	public static ValidationType getByKey(Long key) {
		ValidationType[] types = values();
		for (ValidationType item: types) {
			if (item.getKey().equals(key)) {
				return item;
			}
		}
		
		return null;
	}

	public static ValidationType getByType(String type) {
		ValidationType[] types = values();
		for (ValidationType item: types) {
			if (item.getType().equals(type)) {
				return item;
			}
		}
		
		return null;
	}
	
	public Long getKey() {
		return key;
	}

	public void setKey(Long key) {
		this.key = key;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
}
