package grupoHasten.b2bvo.application.enumerates;

public enum AttachmetType {
	
	Imagen("img", "imagen"),
	Document("pdf", "documento");
//	Imagen("1", "imagen"),
//	Document("2", "documento");
	
	private String key;
	private String type;
	
	private AttachmetType(String key, String type) {
		this.key = key;
		this.type = type;
	}

	public static AttachmetType getByKey(String key) {
		AttachmetType[] types = values();
		for (AttachmetType item: types) {
			if (item.getKey().equals(key)) {
				return item;
			}
		}
		return null;
	}
	
	public static AttachmetType getByType(String type) {
		AttachmetType[] types = values();
		for (AttachmetType item: types) {
			if (item.getType().equals(type)) {
				return item;
			}
		}
		return null;
		
	}
	
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	

}
