package grupoHasten.b2bvo.application.enumerates;

public enum NegotiationState {
	
	Request(0L, "Solicitud"),
	Reservation(1L, "Reserva"),
	Management(2L, "Administracion"),
	Negotiation(3L, "Negociación"),
	Archived(4L, "Archived");
	
	private Long key;
	private String state;
	
	NegotiationState(Long key, String state) {
		this.key = key;
		this.state = state;
	}
	
	public static NegotiationState getByKey(Long key) {
		NegotiationState[] states = NegotiationState.values();
		
		for(NegotiationState item: states) {
			if (item.getKey().equals(key)) {
				return item;
			}
		}
		
		return null;
	}

	public static NegotiationState getByState(String state) {
		NegotiationState[] states = NegotiationState.values();
		
		for(NegotiationState item: states) {
			if (item.getState().contentEquals(state)) {
				return item;
			}
		}
		
		return null;		
	}

	public Long getKey() {
		return key;
	}

	public void setKey(Long key) {
		this.key = key;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

}
